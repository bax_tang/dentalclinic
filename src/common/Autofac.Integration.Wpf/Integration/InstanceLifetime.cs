﻿using System;

namespace Autofac.Integration.Wpf
{
	public enum InstanceLifetime
	{
		/// <summary>
		/// Single instance
		/// </summary>
		Single,

		/// <summary>
		/// Instance per lifetime scope
		/// </summary>
		Scoped,

		/// <summary>
		/// Instance per request
		/// </summary>
		Transient
	}
}