﻿using System;
using System.Windows.Mvvm;
using System.Windows.Navigation;

namespace Autofac.Integration.Wpf
{
	internal class DefaultNavigationService : INavigationService
	{
		private readonly ILifetimeScope _lifetimeScope;
		private readonly NavigationService _navigationService;

		internal DefaultNavigationService(ILifetimeScope lifetimeScope, NavigationService navigationService)
		{
			_lifetimeScope = lifetimeScope;
			_navigationService = navigationService;
		}

		public bool NavigateToViewModel<TViewModel>() where TViewModel : IViewModel
		{
			var viewModel = _lifetimeScope.Resolve<TViewModel>();

			return _navigationService.Navigate(viewModel.AttachedView);
		}

		public bool NavigateToViewModel<TViewModel, TParameter>(TParameter parameter) where TViewModel : IViewModel<TParameter>
		{
			var viewModel = _lifetimeScope.Resolve<TViewModel>();
			viewModel.Parameter = parameter;

			return _navigationService.Navigate(viewModel.AttachedView);
		}

		public bool? NavigateToModalViewModel<TModalViewModel>(bool showModal = false) where TModalViewModel : IModalViewModel
		{
			var viewModel = _lifetimeScope.Resolve<TModalViewModel>();

			if (showModal)
			{
				return viewModel.AttachedView.ShowDialog();
			}

			viewModel.AttachedView.Show();
			return true;
		}

		public bool? NavigateToModalViewModel<TModalViewModel, TParameter>(TParameter parameter, bool showModal = false)
			where TModalViewModel : IModalViewModel<TParameter>
		{
			var viewModel = _lifetimeScope.Resolve<TModalViewModel>();
			viewModel.Parameter = parameter;

			if (showModal)
			{
				return viewModel.AttachedView.ShowDialog();
			}

			viewModel.AttachedView.Show();
			return true;
		}
	}
}