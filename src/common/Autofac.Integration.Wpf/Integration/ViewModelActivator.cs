﻿using System;
using System.Collections.Generic;
using System.Windows.Mvvm;

using Autofac.Core;

namespace Autofac.Integration.Wpf
{
	internal interface IViewModelActivator { }

	internal class BaseViewModelActivator : IViewModelActivator
	{
		private static List<IViewModelActivator> _createdActivators;

		static BaseViewModelActivator()
		{
			_createdActivators = new List<IViewModelActivator>(4);
		}

		public BaseViewModelActivator()
		{
			_createdActivators.Add(this);
		}
	}

	internal class ViewModelActivator<TViewModel, TView> : BaseViewModelActivator where TViewModel : IViewModel where TView : IView
	{
		public ViewModelActivator() : base() { }

		public void Activate(IActivatingEventArgs<TViewModel> args)
		{
			var boundedView = args.Context.Resolve<TView>();

			args.Instance.AttachView(boundedView);
		}
	}

	internal class ModalViewModelActivator<TViewModel, TView> : BaseViewModelActivator where TViewModel : IModalViewModel where TView : IModalView
	{
		public ModalViewModelActivator() : base() { }

		public void Activate(IActivatingEventArgs<TViewModel> args)
		{
			var boundedView = args.Context.Resolve<TView>();

			args.Instance.AttachView(boundedView);
		}
	};
}