﻿using System;

namespace System.Windows.Input
{
	public class DelegateCommand : CommandBase
	{
		#region Fields

		private Func<bool> _canExecute;

		private Action _execute;
		#endregion

		#region Constructors

		public DelegateCommand(Action execute, Func<bool> canExecute = null)
		{
			if (execute == null)
			{
				throw new ArgumentNullException(nameof(execute), "Execute delegate cannot be null.");
			}

			_execute = execute;
			_canExecute = canExecute ?? AlwaysTrue;
		}
		#endregion

		#region CommandBase methods overriding

		public override bool CanExecute(object parameter)
		{
			return _canExecute.Invoke();
		}

		public override void Execute(object parameter)
		{
			_execute.Invoke();
		}
		#endregion

		#region Private class methods

		private static bool AlwaysTrue() => true;
		#endregion
	}
}