﻿using System;

namespace System.Windows.Input
{
	public class DelegateCommand<TParameter> : CommandBase where TParameter : class
	{
		private Func<TParameter, bool> _canExecute;

		private Action<TParameter> _execute;

		public DelegateCommand(Action<TParameter> execute, Func<TParameter, bool> canExecute = null)
		{
			if (execute == null)
			{
				throw new ArgumentNullException(nameof(execute), "Execute delegate cannot be null.");
			}

			_execute = execute;
			_canExecute = canExecute ?? AlwaysTrue;
		}

		public override bool CanExecute(object parameter)
		{
			var p = parameter as TParameter;
			if (p != null)
				return _canExecute.Invoke(p);

			return false;
		}

		public override void Execute(object parameter)
		{
			_execute.Invoke(parameter as TParameter);
		}

		private static bool AlwaysTrue(TParameter parameter) => true;
	}
}