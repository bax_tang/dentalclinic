﻿using System;

namespace System.Windows.Mvvm
{
	public interface IViewModel<TParameter> : IViewModel
	{
		TParameter Parameter { get; set; }
	}
}