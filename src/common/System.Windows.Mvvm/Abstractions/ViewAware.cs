﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace System.Windows.Mvvm
{
	public abstract class ViewAware<TView> : PropertyChangedBase, IViewAware<TView> where TView : IView
	{
		private TView _attachedView;
		private Dispatcher _currentDispatcher;

		public TView AttachedView => _attachedView;

		protected internal ViewAware() : base()
		{
			_currentDispatcher = Dispatcher.CurrentDispatcher;
		}

		public void AttachView(TView view)
		{
			if (view == null)
			{
				throw new ArgumentNullException(nameof(view));
			}

			_attachedView = view;

			OnActivating();

			view.DataContext = this;
			view.Loaded += HandleViewLoaded;
			view.Unloaded += HandleViewUnloaded;
		}

		private void HandleViewLoaded(object sender, RoutedEventArgs args)
		{
			_attachedView.Loaded -= HandleViewLoaded;

			OnViewLoaded();
		}

		private void HandleViewUnloaded(object sender, RoutedEventArgs args)
		{
			_attachedView.Unloaded -= HandleViewUnloaded;

			OnViewUnloaded();
		}

		protected virtual void OnActivating() { }

		protected virtual void OnViewLoaded() { }

		protected virtual void OnViewUnloaded() { }

		protected virtual void ExecuteOnUiThread(Action callback)
		{
			_currentDispatcher.BeginInvoke(callback, DispatcherPriority.Normal);
		}

		protected virtual async Task ExecuteOnUiThreadAsync(Action callback)
		{
			var operation = _currentDispatcher.BeginInvoke(callback, DispatcherPriority.Normal);
			await operation;
		}
	}
}