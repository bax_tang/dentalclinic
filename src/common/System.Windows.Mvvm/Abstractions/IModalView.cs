﻿using System;

namespace System.Windows.Mvvm
{
	public interface IModalView : IView
	{
		bool? DialogResult { get; set; }

		void Close();

		void Show();

		bool? ShowDialog();
	}
}