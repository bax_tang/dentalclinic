﻿using System;

namespace System.Windows.Mvvm
{
	public interface IViewAware { }

	public interface IViewAware<TView> : IViewAware where TView : IView
	{
		TView AttachedView { get; }

		void AttachView(TView view);
	}
}