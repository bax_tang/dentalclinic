﻿using System;

namespace System.Windows.Mvvm
{
	public interface IModalViewModel<TParameter> : IModalViewModel
	{
		TParameter Parameter { get; set; }
	}
}