﻿using System;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace System.Windows.Mvvm
{
	public abstract class ViewModelBase : ViewAware<IView>, IViewModel
	{
		protected internal ViewModelBase() : base() { }
	}
}