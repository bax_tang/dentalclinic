﻿using System;

namespace System.Windows.Mvvm
{
	public abstract class ModalViewModelBase : ViewAware<IModalView>, IModalViewModel
	{
		protected internal ModalViewModelBase() : base() { }
	}
}