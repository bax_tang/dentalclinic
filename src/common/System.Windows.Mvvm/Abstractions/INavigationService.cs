﻿using System;
using System.Windows;

namespace System.Windows.Mvvm
{
	public interface INavigationService
	{
		bool NavigateToViewModel<TViewModel>() where TViewModel : IViewModel;

		bool NavigateToViewModel<TViewModel, TParameter>(TParameter parameter) where TViewModel : IViewModel<TParameter>;

		bool? NavigateToModalViewModel<TModalViewModel>(bool showModal = false) where TModalViewModel : IModalViewModel;

		bool? NavigateToModalViewModel<TModalViewModel, TParameter>(TParameter parameter, bool showModal = false)
			where TModalViewModel : IModalViewModel<TParameter>;
	}
}