﻿using System;

namespace System.Windows.Mvvm
{
	public interface IModalViewModel : IViewAware<IModalView> { }
}