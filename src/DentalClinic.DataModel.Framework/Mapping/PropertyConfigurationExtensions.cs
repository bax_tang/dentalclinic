﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration.Configuration;

namespace DentalClinic.DataModel.Mapping
{
	public static class PropertyConfigurationExtensions
	{
		public static TPropertyConfiguration HasIndex<TPropertyConfiguration>(
			this TPropertyConfiguration propertyConfiguration,
			bool clustered = false,
			bool unique = false,
			string name = null,
			int? order = null) where TPropertyConfiguration : PrimitivePropertyConfiguration
		{
			propertyConfiguration.HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute(name)
			{
				IsClustered = clustered,
				IsUnique = unique,
				Order = order.GetValueOrDefault(0)
			}));

			return propertyConfiguration;
		}
	}
}