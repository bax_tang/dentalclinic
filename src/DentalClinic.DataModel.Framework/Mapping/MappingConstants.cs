﻿using System;

namespace DentalClinic.DataModel.Mapping
{
	internal static class MappingConstants
	{
		public const int IdColumnOrder = 0;
		public const int InitialColumnOrder = 1;
		public const int CreatedAtColumnOrder = 1000;
		public const int UpdatedAtColumnOrder = 1001;

		public const int ExtraSmallLength = 16;
		public const int SmallLength = 32;
		public const int DefaultLength = 64;
		public const int MediumLength = 128;

		public const string DateTimeColumnType = "DATETIME2";
		public const string MoneyColumnType = "MONEY";

		public const string SqlDefaultValueAnnotation = "SqlDefaultValue";
	}
}