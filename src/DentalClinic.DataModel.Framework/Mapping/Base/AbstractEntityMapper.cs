﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;

namespace DentalClinic.DataModel.Mapping
{
	public abstract class AbstractEntityMapper<TEntity> : IEntityMapper where TEntity : class
	{
		protected readonly string _tableName;

		protected internal AbstractEntityMapper(string tableName)
		{
			if (string.IsNullOrEmpty(tableName))
			{
				throw new ArgumentNullException(nameof(tableName), "Table name cannot be null or empty.");
			}

			_tableName = tableName;
		}

		public void Map(DbModelBuilder modelBuilder)
		{
			var entityConfig = modelBuilder.Entity<TEntity>();

			entityConfig.ToTable(_tableName, "dbo");

			MapEntity(entityConfig);
		}

		protected virtual void MapEntity(EntityTypeConfiguration<TEntity> entityConfig) { }
	}
}