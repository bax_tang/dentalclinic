﻿using System;
using System.Data.Entity;

namespace DentalClinic.DataModel.Mapping
{
	public interface IEntityMapper
	{
		void Map(DbModelBuilder modelBuilder);
	}
}