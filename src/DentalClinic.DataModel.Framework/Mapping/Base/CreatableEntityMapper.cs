﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

using DentalClinic.DataModel.Entities;

namespace DentalClinic.DataModel.Mapping
{
	public abstract class CreatableEntityMapper<TEntity> : BaseEntityMapper<TEntity> where TEntity : CreatableEntity
	{
		protected internal CreatableEntityMapper(string tableName) : base(tableName) { }

		protected override void MapEntity(EntityTypeConfiguration<TEntity> entityConfig)
		{
			base.MapEntity(entityConfig);

			entityConfig.Property(x => x.CreatedAt)
				.HasColumnOrder(MappingConstants.CreatedAtColumnOrder)
				.HasColumnType(MappingConstants.DateTimeColumnType)
				.HasColumnAnnotation("SqlDefaultValue", "GETDATE()")
				.IsOptional();
		}
	}
}