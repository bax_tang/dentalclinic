﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;

using DentalClinic.DataModel.Entities;

namespace DentalClinic.DataModel.Mapping
{
	public abstract class BaseEntityMapper<TEntity> : AbstractEntityMapper<TEntity> where TEntity : BaseEntity
	{
		protected internal BaseEntityMapper(string tableName) : base(tableName) { }

		protected override void MapEntity(EntityTypeConfiguration<TEntity> entityConfig)
		{
			base.MapEntity(entityConfig);

			entityConfig.Property(x => x.Id)
				.HasColumnOrder(MappingConstants.IdColumnOrder)
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity)
				.IsRequired();

			entityConfig.HasKey(x => x.Id, ConfigurePrimaryKey);
		}

		private void ConfigurePrimaryKey(PrimaryKeyIndexConfiguration keyConfig)
		{
			keyConfig.HasName(BuildPrimaryKeyName()).IsClustered(false);
		}

		private string BuildPrimaryKeyName()
		{
			return string.Concat(_tableName, "_pk_id").ToLowerInvariant();
		}
	}
}