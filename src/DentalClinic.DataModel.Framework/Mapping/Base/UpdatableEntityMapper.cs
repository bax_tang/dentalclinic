﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

using DentalClinic.DataModel.Entities;

namespace DentalClinic.DataModel.Mapping
{
	public abstract class UpdatableEntityMapper<TEntity> : CreatableEntityMapper<TEntity> where TEntity : UpdatableEntity
	{
		protected internal UpdatableEntityMapper(string tableName) : base(tableName) { }

		protected override void MapEntity(EntityTypeConfiguration<TEntity> entityConfig)
		{
			base.MapEntity(entityConfig);

			entityConfig.Property(x => x.UpdatedAt)
				.HasColumnOrder(MappingConstants.UpdatedAtColumnOrder)
				.HasColumnType(MappingConstants.DateTimeColumnType)
				.IsOptional();
		}
	}
}