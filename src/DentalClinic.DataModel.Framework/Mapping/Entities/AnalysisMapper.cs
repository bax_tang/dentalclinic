﻿using System;
using System.Data.Entity.ModelConfiguration;

using DentalClinic.DataModel.Entities;

namespace DentalClinic.DataModel.Mapping
{
	public class AnalysisMapper : UpdatableEntityMapper<Analysis>
	{
		public AnalysisMapper() : base("Analyzes") { }

		protected override void MapEntity(EntityTypeConfiguration<Analysis> entityConfig)
		{
			base.MapEntity(entityConfig);

			int columnOrder = MappingConstants.InitialColumnOrder;

			entityConfig.Property(x => x.WorkKind)
				.HasColumnOrder(columnOrder++)
				.HasMaxLength(MappingConstants.MediumLength)
				.IsUnicode(true)
				.IsRequired();

			entityConfig.Property(x => x.Price)
				.HasColumnOrder(columnOrder++)
				.HasColumnType(MappingConstants.MoneyColumnType)
				.IsRequired();
		}
	}
}