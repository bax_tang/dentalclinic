﻿using System;
using System.Data.Entity.ModelConfiguration;

using DentalClinic.DataModel.Entities;

namespace DentalClinic.DataModel.Mapping
{
	public class DoctorMapper : UpdatableEntityMapper<Doctor>
	{
		public DoctorMapper() : base("Doctors") { }

		protected override void MapEntity(EntityTypeConfiguration<Doctor> entityConfig)
		{
			base.MapEntity(entityConfig);

			int columnOrder = MappingConstants.InitialColumnOrder;

			entityConfig.Property(x => x.FirstName)
				.HasColumnOrder(columnOrder++)
				.HasMaxLength(MappingConstants.SmallLength)
				.IsUnicode(true)
				.IsRequired();

			entityConfig.Property(x => x.MiddleName)
				.HasColumnOrder(columnOrder++)
				.HasMaxLength(MappingConstants.SmallLength)
				.IsUnicode(true)
				.IsOptional();

			entityConfig.Property(x => x.LastName)
				.HasColumnOrder(columnOrder++)
				.HasMaxLength(MappingConstants.SmallLength)
				.IsUnicode(true)
				.IsRequired();

			entityConfig.Property(x => x.Category)
				.HasColumnOrder(columnOrder++)
				.HasMaxLength(MappingConstants.MediumLength)
				.IsUnicode(true)
				.IsRequired();

			entityConfig.Property(x => x.Seniority)
				.HasColumnOrder(columnOrder++)
				.IsRequired();
		}
	}
}