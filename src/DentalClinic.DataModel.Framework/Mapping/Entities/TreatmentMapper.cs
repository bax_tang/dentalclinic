﻿using System;
using System.Data.Entity.ModelConfiguration;

using DentalClinic.DataModel.Entities;

namespace DentalClinic.DataModel.Mapping
{
	public class TreatmentMapper : CreatableEntityMapper<Treatment>
	{
		public TreatmentMapper() : base("Treatments") { }

		protected override void MapEntity(EntityTypeConfiguration<Treatment> entityConfig)
		{
			base.MapEntity(entityConfig);

			int columnOrder = MappingConstants.InitialColumnOrder;

			entityConfig.Property(x => x.AppointmentId)
				.HasColumnOrder(columnOrder++)
				.IsRequired();

			entityConfig.Property(x => x.AnalysisId)
				.HasColumnOrder(columnOrder++)
				.IsRequired();

			entityConfig.Property(x => x.DiagnosisId)
				.HasColumnOrder(columnOrder++)
				.IsRequired();

			entityConfig.Property(x => x.ProcedureId)
				.HasColumnOrder(columnOrder++)
				.IsRequired();

			entityConfig.Property(x => x.Comments)
				.HasColumnOrder(columnOrder++)
				.IsMaxLength()
				.IsOptional();

			entityConfig.HasRequired(x => x.Appointment)
				.WithMany()
				.HasForeignKey(x => x.AppointmentId)
				.WillCascadeOnDelete(false);

			entityConfig.HasRequired(x => x.Analysis)
				.WithMany()
				.HasForeignKey(x => x.AnalysisId)
				.WillCascadeOnDelete(false);

			entityConfig.HasRequired(x => x.Diagnosis)
				.WithMany()
				.HasForeignKey(x => x.DiagnosisId)
				.WillCascadeOnDelete(false);

			entityConfig.HasRequired(x => x.Procedure)
				.WithMany()
				.HasForeignKey(x => x.ProcedureId)
				.WillCascadeOnDelete(false);
		}
	}
}