﻿using System;
using System.Data.Entity.ModelConfiguration;

using DentalClinic.DataModel.Entities;

namespace DentalClinic.DataModel.Mapping
{
	public class ProcedureMapper : UpdatableEntityMapper<Procedure>
	{
		public ProcedureMapper() : base("Procedures") { }

		protected override void MapEntity(EntityTypeConfiguration<Procedure> entityConfig)
		{
			base.MapEntity(entityConfig);

			int columnOrder = MappingConstants.InitialColumnOrder;

			entityConfig.Property(x => x.Name)
				.HasColumnOrder(columnOrder++)
				.HasMaxLength(MappingConstants.DefaultLength)
				.IsUnicode(true)
				.IsRequired();

			entityConfig.Property(x => x.Price)
				.HasColumnOrder(columnOrder++)
				.HasColumnType(MappingConstants.MoneyColumnType)
				.IsRequired();

			entityConfig.Property(x => x.MedicamentId)
				.HasColumnOrder(columnOrder++)
				.IsOptional();

			entityConfig.HasOptional(x => x.Medicament)
				.WithMany()
				.HasForeignKey(x => x.MedicamentId)
				.WillCascadeOnDelete(false);
		}
	}
}