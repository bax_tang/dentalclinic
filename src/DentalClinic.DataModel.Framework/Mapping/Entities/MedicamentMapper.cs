﻿using System;
using System.Data.Entity.ModelConfiguration;

using DentalClinic.DataModel.Entities;

namespace DentalClinic.DataModel.Mapping
{
	public class MedicamentMapper : UpdatableEntityMapper<Medicament>
	{
		public MedicamentMapper() : base("Medicaments") { }

		protected override void MapEntity(EntityTypeConfiguration<Medicament> entityConfig)
		{
			base.MapEntity(entityConfig);

			int columnOrder = MappingConstants.InitialColumnOrder;

			entityConfig.Property(x => x.Name)
				.HasColumnOrder(columnOrder++)
				.HasMaxLength(MappingConstants.DefaultLength)
				.IsUnicode(true)
				.IsRequired();

			entityConfig.Property(x => x.Dosage)
				.HasColumnOrder(columnOrder++)
				.HasMaxLength(MappingConstants.DefaultLength)
				.IsUnicode(true)
				.IsRequired();

			entityConfig.Property(x => x.Price)
				.HasColumnOrder(columnOrder++)
				.HasColumnType(MappingConstants.MoneyColumnType)
				.IsRequired();
		}
	}
}