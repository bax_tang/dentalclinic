﻿using System;
using System.Data.Entity.ModelConfiguration;

using DentalClinic.DataModel.Entities;

namespace DentalClinic.DataModel.Mapping
{
	public class AppointmentMapper : UpdatableEntityMapper<Appointment>
	{
		public AppointmentMapper() : base("Appointments") { }

		protected override void MapEntity(EntityTypeConfiguration<Appointment> entityConfig)
		{
			base.MapEntity(entityConfig);

			int columnOrder = MappingConstants.InitialColumnOrder;

			entityConfig.Property(x => x.Date)
				.HasColumnOrder(columnOrder++)
				.HasColumnType(MappingConstants.DateTimeColumnType)
				.IsRequired();

			entityConfig.Property(x => x.DoctorId)
				.HasColumnOrder(columnOrder++)
				.IsRequired();

			entityConfig.Property(x => x.PatientId)
				.HasColumnOrder(columnOrder++)
				.IsRequired();

			entityConfig.HasRequired(x => x.Doctor)
				.WithMany()
				.HasForeignKey(x => x.DoctorId)
				.WillCascadeOnDelete(false);

			entityConfig.HasRequired(x => x.Patient)
				.WithMany()
				.HasForeignKey(x => x.PatientId)
				.WillCascadeOnDelete(false);
		}
	}
}