﻿using System;
using System.Data.Entity.ModelConfiguration;

using DentalClinic.DataModel.Entities;

namespace DentalClinic.DataModel.Mapping
{
	public class PatientMapper : UpdatableEntityMapper<Patient>
	{
		public PatientMapper() : base("Patients") { }

		protected override void MapEntity(EntityTypeConfiguration<Patient> entityConfig)
		{
			base.MapEntity(entityConfig);

			int columnOrder = MappingConstants.InitialColumnOrder;

			entityConfig.Property(x => x.FirstName)
				.HasColumnOrder(columnOrder++)
				.HasMaxLength(MappingConstants.SmallLength)
				.IsUnicode(true)
				.IsRequired();

			entityConfig.Property(x => x.MiddleName)
				.HasColumnOrder(columnOrder++)
				.HasMaxLength(MappingConstants.SmallLength)
				.IsUnicode(true)
				.IsOptional();

			entityConfig.Property(x => x.LastName)
				.HasColumnOrder(columnOrder++)
				.HasMaxLength(MappingConstants.SmallLength)
				.IsUnicode(true)
				.IsRequired();

			entityConfig.Property(x => x.BirthDate)
				.HasColumnOrder(columnOrder++)
				.HasColumnType(MappingConstants.DateTimeColumnType)
				.IsRequired();

			entityConfig.Property(x => x.PolicyNumber)
				.HasColumnOrder(columnOrder++)
				.HasMaxLength(MappingConstants.ExtraSmallLength)
				.HasIndex(name: "patients_uni_policynumber", clustered: false, unique: true)
				.IsUnicode(true)
				.IsRequired();

			entityConfig.Property(x => x.Address)
				.HasColumnOrder(columnOrder++)
				.HasMaxLength(MappingConstants.MediumLength)
				.IsUnicode(true)
				.IsOptional();

			entityConfig.Property(x => x.Phone)
				.HasColumnOrder(columnOrder++)
				.HasMaxLength(MappingConstants.ExtraSmallLength)
				.IsUnicode(true)
				.IsOptional();
		}
	}
}