﻿using System;
using System.Data.Entity.ModelConfiguration;

using DentalClinic.DataModel.Entities;

namespace DentalClinic.DataModel.Mapping
{
	public class DiagnosisMapper : UpdatableEntityMapper<Diagnosis>
	{
		public DiagnosisMapper() : base("Diagnoses") { }

		protected override void MapEntity(EntityTypeConfiguration<Diagnosis> entityConfig)
		{
			base.MapEntity(entityConfig);

			int columnOrder = MappingConstants.InitialColumnOrder;

			entityConfig.Property(x => x.Type)
				.HasColumnOrder(columnOrder++)
				.HasMaxLength(MappingConstants.DefaultLength)
				.IsUnicode(true)
				.IsRequired();

			entityConfig.Property(x => x.Name)
				.HasColumnOrder(columnOrder++)
				.HasMaxLength(MappingConstants.DefaultLength)
				.IsUnicode(true)
				.IsRequired();
		}
	}
}