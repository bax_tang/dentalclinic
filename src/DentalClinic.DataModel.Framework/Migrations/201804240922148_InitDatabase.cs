namespace DentalClinic.DataModel.Migrations
{
	using System;
	using System.Collections.Generic;
	using System.Data.Entity.Infrastructure.Annotations;
	using System.Data.Entity.Migrations;

	public partial class InitDatabase : DbMigration
	{
		public override void Up()
		{
			CreateTable(
				"dbo.Patients",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					FirstName = c.String(nullable: false, maxLength: 32),
					MiddleName = c.String(maxLength: 32),
					LastName = c.String(nullable: false, maxLength: 32),
					BirthDate = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
					PolicyNumber = c.String(nullable: false, maxLength: 16),
					Address = c.String(maxLength: 128),
					Phone = c.String(maxLength: 16),
					CreatedAt = c.DateTime(precision: 7, storeType: "datetime2",
							annotations: new Dictionary<string, AnnotationValues>
							{
								{
									"SqlDefaultValue",
									new AnnotationValues(oldValue: null, newValue: "GETDATE()")
								},
							}),
					UpdatedAt = c.DateTime(precision: 7, storeType: "datetime2"),
				})
				.PrimaryKey(t => t.Id, name: "patients_pk_id", clustered: false)
				.Index(t => t.PolicyNumber, unique: true, name: "patients_uni_policynumber");

			CreateTable(
				"dbo.Doctors",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					FirstName = c.String(nullable: false, maxLength: 32),
					MiddleName = c.String(maxLength: 32),
					LastName = c.String(nullable: false, maxLength: 32),
					Category = c.String(nullable: false, maxLength: 128),
					Seniority = c.Int(nullable: false),
					CreatedAt = c.DateTime(precision: 7, storeType: "datetime2",
							annotations: new Dictionary<string, AnnotationValues>
							{
								{
									"SqlDefaultValue",
									new AnnotationValues(oldValue: null, newValue: "GETDATE()")
								},
							}),
					UpdatedAt = c.DateTime(precision: 7, storeType: "datetime2"),
				})
				.PrimaryKey(t => t.Id, name: "doctors_pk_id", clustered: false);

			CreateTable(
				"dbo.Analyzes",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					WorkKind = c.String(nullable: false, maxLength: 128),
					Price = c.Decimal(nullable: false, storeType: "money"),
					CreatedAt = c.DateTime(precision: 7, storeType: "datetime2",
							annotations: new Dictionary<string, AnnotationValues>
							{
								{
									"SqlDefaultValue",
									new AnnotationValues(oldValue: null, newValue: "GETDATE()")
								},
							}),
					UpdatedAt = c.DateTime(precision: 7, storeType: "datetime2"),
				})
				.PrimaryKey(t => t.Id, name: "analyzes_pk_id", clustered: false);

			CreateTable(
				"dbo.Medicaments",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(nullable: false, maxLength: 64),
					Dosage = c.String(nullable: false, maxLength: 64),
					Price = c.Decimal(nullable: false, storeType: "money"),
					CreatedAt = c.DateTime(precision: 7, storeType: "datetime2",
							annotations: new Dictionary<string, AnnotationValues>
							{
								{
									"SqlDefaultValue",
									new AnnotationValues(oldValue: null, newValue: "GETDATE()")
								},
							}),
					UpdatedAt = c.DateTime(precision: 7, storeType: "datetime2"),
				})
				.PrimaryKey(t => t.Id, name: "medicaments_pk_id", clustered: false);

			CreateTable(
				"dbo.Diagnoses",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Type = c.String(nullable: false, maxLength: 64),
					Name = c.String(nullable: false, maxLength: 64),
					CreatedAt = c.DateTime(precision: 7, storeType: "datetime2",
							annotations: new Dictionary<string, AnnotationValues>
							{
								{
									"SqlDefaultValue",
									new AnnotationValues(oldValue: null, newValue: "GETDATE()")
								},
							}),
					UpdatedAt = c.DateTime(precision: 7, storeType: "datetime2"),
				})
				.PrimaryKey(t => t.Id, name: "diagnoses_pk_id", clustered: false);

			CreateTable(
				"dbo.Appointments",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Date = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
					DoctorId = c.Long(nullable: false),
					PatientId = c.Long(nullable: false),
					CreatedAt = c.DateTime(precision: 7, storeType: "datetime2",
							annotations: new Dictionary<string, AnnotationValues>
							{
								{
									"SqlDefaultValue",
									new AnnotationValues(oldValue: null, newValue: "GETDATE()")
								},
							}),
					UpdatedAt = c.DateTime(precision: 7, storeType: "datetime2"),
				})
				.PrimaryKey(t => t.Id, name: "appointments_pk_id", clustered: false)
				.ForeignKey("dbo.Doctors", t => t.DoctorId)
				.ForeignKey("dbo.Patients", t => t.PatientId)
				.Index(t => t.DoctorId)
				.Index(t => t.PatientId);

			CreateTable(
				"dbo.Procedures",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					Name = c.String(nullable: false, maxLength: 64),
					Price = c.Decimal(nullable: false, storeType: "money"),
					MedicamentId = c.Long(),
					CreatedAt = c.DateTime(precision: 7, storeType: "datetime2",
							annotations: new Dictionary<string, AnnotationValues>
							{
								{
									"SqlDefaultValue",
									new AnnotationValues(oldValue: null, newValue: "GETDATE()")
								},
							}),
					UpdatedAt = c.DateTime(precision: 7, storeType: "datetime2"),
				})
				.PrimaryKey(t => t.Id, name: "procedures_pk_id", clustered: false)
				.ForeignKey("dbo.Medicaments", t => t.MedicamentId)
				.Index(t => t.MedicamentId);

			CreateTable(
				"dbo.Treatments",
				c => new
				{
					Id = c.Long(nullable: false, identity: true),
					AppointmentId = c.Long(nullable: false),
					AnalysisId = c.Long(nullable: false),
					DiagnosisId = c.Long(nullable: false),
					ProcedureId = c.Long(nullable: false),
					Comments = c.String(),
					CreatedAt = c.DateTime(precision: 7, storeType: "datetime2",
							annotations: new Dictionary<string, AnnotationValues>
							{
								{
									"SqlDefaultValue",
									new AnnotationValues(oldValue: null, newValue: "GETDATE()")
								},
							}),
				})
				.PrimaryKey(t => t.Id, name: "treatments_pk_id", clustered: false)
				.ForeignKey("dbo.Analyzes", t => t.AnalysisId)
				.ForeignKey("dbo.Appointments", t => t.AppointmentId)
				.ForeignKey("dbo.Diagnoses", t => t.DiagnosisId)
				.ForeignKey("dbo.Procedures", t => t.ProcedureId)
				.Index(t => t.AppointmentId)
				.Index(t => t.AnalysisId)
				.Index(t => t.DiagnosisId)
				.Index(t => t.ProcedureId);

		}

		public override void Down()
		{
			DropForeignKey("dbo.Treatments", "ProcedureId", "dbo.Procedures");
			DropForeignKey("dbo.Treatments", "DiagnosisId", "dbo.Diagnoses");
			DropForeignKey("dbo.Treatments", "AppointmentId", "dbo.Appointments");
			DropForeignKey("dbo.Treatments", "AnalysisId", "dbo.Analyzes");
			DropForeignKey("dbo.Procedures", "MedicamentId", "dbo.Medicaments");
			DropForeignKey("dbo.Appointments", "PatientId", "dbo.Patients");
			DropForeignKey("dbo.Appointments", "DoctorId", "dbo.Doctors");
			DropIndex("dbo.Treatments", new[] { "ProcedureId" });
			DropIndex("dbo.Treatments", new[] { "DiagnosisId" });
			DropIndex("dbo.Treatments", new[] { "AnalysisId" });
			DropIndex("dbo.Treatments", new[] { "AppointmentId" });
			DropIndex("dbo.Procedures", new[] { "MedicamentId" });
			DropIndex("dbo.Appointments", new[] { "PatientId" });
			DropIndex("dbo.Appointments", new[] { "DoctorId" });
			DropIndex("dbo.Patients", "patients_uni_policynumber");
			DropTable("dbo.Treatments",
				removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
				{
					{
						"CreatedAt",
						new Dictionary<string, object>
						{
							{ "SqlDefaultValue", "GETDATE()" },
						}
					},
				});
			DropTable("dbo.Procedures",
				removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
				{
					{
						"CreatedAt",
						new Dictionary<string, object>
						{
							{ "SqlDefaultValue", "GETDATE()" },
						}
					},
				});
			DropTable("dbo.Appointments",
				removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
				{
					{
						"CreatedAt",
						new Dictionary<string, object>
						{
							{ "SqlDefaultValue", "GETDATE()" },
						}
					},
				});
			DropTable("dbo.Diagnoses",
				removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
				{
					{
						"CreatedAt",
						new Dictionary<string, object>
						{
							{ "SqlDefaultValue", "GETDATE()" },
						}
					},
				});
			DropTable("dbo.Medicaments",
				removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
				{
					{
						"CreatedAt",
						new Dictionary<string, object>
						{
							{ "SqlDefaultValue", "GETDATE()" },
						}
					},
				});
			DropTable("dbo.Analyzes",
				removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
				{
					{
						"CreatedAt",
						new Dictionary<string, object>
						{
							{ "SqlDefaultValue", "GETDATE()" },
						}
					},
				});
			DropTable("dbo.Doctors",
				removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
				{
					{
						"CreatedAt",
						new Dictionary<string, object>
						{
							{ "SqlDefaultValue", "GETDATE()" },
						}
					},
				});
			DropTable("dbo.Patients",
				removedColumnAnnotations: new Dictionary<string, IDictionary<string, object>>
				{
					{
						"CreatedAt",
						new Dictionary<string, object>
						{
							{ "SqlDefaultValue", "GETDATE()" },
						}
					},
				});
		}
	}
}
