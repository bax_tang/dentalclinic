﻿using System;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Interception;
using System.Text.RegularExpressions;

namespace DentalClinic.DataModel.Migrations
{
	public class DentalClinicCollationInterceptor : IDbCommandInterceptor
	{
		private readonly string _collation;

		public DentalClinicCollationInterceptor(string collation)
		{
			if (string.IsNullOrEmpty(collation))
			{
				throw new ArgumentNullException(nameof(collation), "Collation name cannot be null.");
			}

			_collation = collation;
		}

		public void NonQueryExecuted(DbCommand command, DbCommandInterceptionContext<int> interceptionContext) { }

		public void NonQueryExecuting(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
		{
			if (Regex.IsMatch(command.CommandText, @"^create database \[.*]$"))
			{
				command.CommandText = string.Concat(command.CommandText, " COLLATE ", _collation);
			}
		}

		public void ReaderExecuted(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext) { }

		public void ReaderExecuting(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext) { }

		public void ScalarExecuted(DbCommand command, DbCommandInterceptionContext<object> interceptionContext) { }

		public void ScalarExecuting(DbCommand command, DbCommandInterceptionContext<object> interceptionContext) { }
	}
}