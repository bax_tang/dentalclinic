﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.Migrations.Model;
using System.Data.Entity.Migrations.Sql;
using System.Data.Entity.SqlServer;

namespace DentalClinic.DataModel.Migrations
{
	internal class DentalClinicMigrationSqlGenerator : SqlServerMigrationSqlGenerator
	{
		private const string DropConstraintQuery = @"
DECLARE @constraint_{0} NVARCHAR(128) = NULL;

SELECT @constraint_{0} = [name]
FROM [sys].[default_constraints]
WHERE
	[parent_object_id] = OBJECT_ID(N'[{1}].[{2}]', 'U')
	AND COL_NAME([parent_object_id], [parent_column_id]) = '{3}';

IF @constraint_{0} IS NOT NULL
	EXECUTE('ALTER TABLE [{1}].[{2}] DROP CONSTRAINT [' + @constraint_{0} + ']');
";

		private readonly char[] _tableNameSplitters = new[] { '.' };
		private int _dropConstraintCount = 0;

		public DentalClinicMigrationSqlGenerator() : base() { }

		protected override void Generate(AddColumnOperation addColumnOperation)
		{
			SetAnnotatedColumn(addColumnOperation.Column, addColumnOperation.Table);

			base.Generate(addColumnOperation);
		}

		protected override void Generate(AlterColumnOperation alterColumnOperation)
		{
			SetAnnotatedColumn(alterColumnOperation.Column, alterColumnOperation.Table);

			base.Generate(alterColumnOperation);
		}

		protected override void Generate(CreateTableOperation createTableOperation)
		{
			SetAnnotatedColumns(createTableOperation.Columns, createTableOperation.Name);

			base.Generate(createTableOperation);
		}

		protected override void Generate(AlterTableOperation alterTableOperation)
		{
			SetAnnotatedColumns(alterTableOperation.Columns, alterTableOperation.Name);

			base.Generate(alterTableOperation);
		}


		private void SetAnnotatedColumn(ColumnModel column, string tableName)
		{
			AnnotationValues values;
			if (column.Annotations.TryGetValue("SqlDefaultValue", out values))
			{
				if (values.NewValue != null)
				{
					column.DefaultValueSql = values.NewValue.ToString();
				}
				else
				{
					column.DefaultValueSql = null;
					using (var writer = Writer())
					{
						writer.WriteLine(GetSqlDropConstraintQuery(tableName, column.Name));
						Statement(writer);
					}
				}
			}
		}

		private void SetAnnotatedColumns(IEnumerable<ColumnModel> columns, string tableName)
		{
			foreach (var column in columns)
			{
				SetAnnotatedColumn(column, tableName);
			}
		}

		private string GetSqlDropConstraintQuery(string tableName, string columnName)
		{
			var splittedTableName = tableName.Split(_tableNameSplitters);

			var tableSchema = splittedTableName[0];
			var pureTableName = splittedTableName[1];

			var queryString = string.Format(DropConstraintQuery, _dropConstraintCount, tableSchema, pureTableName, columnName);

			_dropConstraintCount += 1;

			return queryString;
		}
	}
}