﻿using System;
using System.Data.Entity.Migrations;
using System.Linq.Expressions;

using DentalClinic.DataModel.Entities;
using DentalClinic.DataModel.Framework;

namespace DentalClinic.DataModel.Migrations
{
	public class DentalClinicMigrationsConfiguration : DbMigrationsConfiguration<DentalClinicContext>
	{
		public DentalClinicMigrationsConfiguration()
		{
			AutomaticMigrationsEnabled = false;
			CommandTimeout = 300;
			MigrationsDirectory = "Migrations";
			MigrationsNamespace = "DentalClinic.DataModel.Migrations";

			SetSqlGenerator("System.Data.SqlClient", new DentalClinicMigrationSqlGenerator());
		}

		protected override void Seed(DentalClinicContext context)
		{
			base.Seed(context);

			AddOrUpdateAnalyzes(context);
			AddOrUpdateDiagnoses(context);
			AddOrUpdateDoctors(context);
			AddOrUpdateMedicaments(context);
			AddOrUpdatePatients(context);
			AddOrUpdateProcedures(context);
		}

		private void AddOrUpdateAnalyzes(DentalClinicContext context)
		{
			var analyzesSet = context.Set<Analysis>();

			analyzesSet.AddOrUpdate(x => x.Id,
				new Analysis
				{
					Id = 1,
					WorkKind = "Нет",
					Price = 0M,
					CreatedAt = DateTime.Now
				},
				new Analysis
				{
					Id = 2,
					WorkKind = "Фенотипирование",
					Price = 600M,
					CreatedAt = DateTime.Now
				},
				new Analysis
				{
					Id = 3,
					WorkKind = "Диагностика антифосфолипида",
					Price = 1240M,
					CreatedAt = DateTime.Now
				},
				new Analysis
				{
					Id = 4,
					WorkKind = "Хромогранин A (Chromogranin A, CgA)",
					Price = 1600M,
					CreatedAt = DateTime.Now
				},
				new Analysis
				{
					Id = 5,
					WorkKind = "Вирус простого герпеса (HSV) 1 и 2 типа",
					Price = 1320M,
					CreatedAt = DateTime.Now
				},
				new Analysis
				{
					Id = 6,
					WorkKind = "Диагностика Chlamydia trachomatis",
					Price = 480M,
					CreatedAt = DateTime.Now
				},
				new Analysis
				{
					Id = 7,
					WorkKind = "Диагностика вируса кори Measles",
					Price = 630M,
					CreatedAt = DateTime.Now
				},
				new Analysis
				{
					Id = 8,
					WorkKind = "Общий анализ крови (ОАК)",
					Price = 280M,
					CreatedAt = DateTime.Now
				});
		}

		private void AddOrUpdateDiagnoses(DentalClinicContext context)
		{
			var diagnosesSet = context.Set<Diagnosis>();

			diagnosesSet.AddOrUpdate(x => x.Id,
				new Diagnosis
				{
					Id = 1,
					Type = "Флюс зуба",
					Name = "Одонтогенный периостит",
					CreatedAt = DateTime.Now
				},
				new Diagnosis
				{
					Id = 2,
					Type = "Флюс зуба",
					Name = "Флюс зуба",
					CreatedAt = DateTime.Now
				},
				new Diagnosis
				{
					Id = 3,
					Type = "Боли в зубе",
					Name = "Кариес",
					CreatedAt = DateTime.Now
				},
				new Diagnosis
				{
					Id = 4,
					Type = "Боли в зубе",
					Name = "Пульпит",
					CreatedAt = DateTime.Now
				},
				new Diagnosis
				{
					Id = 5,
					Type = "Боли в зубе",
					Name = "Периодонтит",
					CreatedAt = DateTime.Now
				},
				new Diagnosis
				{
					Id = 6,
					Type = "Боли в зубе",
					Name = "Корневая киста",
					CreatedAt = DateTime.Now
				},
				new Diagnosis
				{
					Id = 7,
					Type = "Боли в зубе",
					Name = "Периапикальный абсцесс",
					CreatedAt = DateTime.Now
				},
				new Diagnosis
				{
					Id = 8,
					Type = "Боли в зубе",
					Name = "Перикоронит",
					CreatedAt = DateTime.Now
				},
				new Diagnosis
				{
					Id = 9,
					Type = "Тетрациклиновые зубы",
					Name = "Реминерализация",
					CreatedAt = DateTime.Now
				});
		}

		private void AddOrUpdateDoctors(DentalClinicContext context)
		{
			var doctorsSet = context.Set<Doctor>();

			doctorsSet.AddOrUpdate(x => x.Id,
				new Doctor
				{
					Id = 1,
					FirstName = "Сергей",
					MiddleName = "Иванович",
					LastName = "Баранов",
					Category = "Директор, зубной врач",
					Seniority = 24,
					CreatedAt = DateTime.Now
				},
				new Doctor
				{
					Id = 2,
					FirstName = "Елена",
					MiddleName = "Игоревна",
					LastName = "Силкова",
					Category = "Главный врач, врач стоматолог-терапевт",
					Seniority = 17,
					CreatedAt = DateTime.Now
				},
				new Doctor
				{
					Id = 3,
					FirstName = "Алексей",
					MiddleName = "Александрович",
					LastName = "Митин",
					Category = "Врач стоматолог ортопед, хирург",
					Seniority = 5,
					CreatedAt = DateTime.Now
				},
				new Doctor
				{
					Id = 4,
					FirstName = "Лариса",
					MiddleName = "Ивановна",
					LastName = "Рываева",
					Category = "Зубной врач",
					Seniority = 12,
					CreatedAt = DateTime.Now
				});
		}

		private void AddOrUpdateMedicaments(DentalClinicContext context)
		{
			var medicamentsSet = context.Set<Medicament>();

			medicamentsSet.AddOrUpdate(x => x.Id,
				new Medicament
				{
					Id = 1,
					Name = "Нет",
					Dosage = "-",
					Price = 0M,
					CreatedAt = DateTime.Now
				},
				new Medicament
				{
					Id = 2,
					Name = "Фторлак",
					Dosage = "13 мл",
					Price = 277M,
					CreatedAt = DateTime.Now
				},
				new Medicament
				{
					Id = 3,
					Name = "Арлет",
					Dosage = "1 г",
					Price = 391M,
					CreatedAt = DateTime.Now
				},
				new Medicament
				{
					Id = 4,
					Name = "АктиГель Мульти-Гин",
					Dosage = "50 мг",
					Price = 824M,
					CreatedAt = DateTime.Now
				},
				new Medicament
				{
					Id = 5,
					Name = "Кларитромицин",
					Dosage = "2 раза в день",
					Price = 229M,
					CreatedAt = DateTime.Now
				},
				new Medicament
				{
					Id = 6,
					Name = "Линкомицин",
					Dosage = "250 мг",
					Price = 140M,
					CreatedAt = DateTime.Now
				},
				new Medicament
				{
					Id = 7,
					Name = "Система отбеливания зубов White Light",
					Dosage = "1 раз в день",
					Price = 369M,
					CreatedAt = DateTime.Now
				});
		}

		private void AddOrUpdatePatients(DentalClinicContext context)
		{
			var patientsSet = context.Set<Patient>();

			patientsSet.AddOrUpdate(x => x.Id,
				new Patient
				{
					Id = 1,
					FirstName = "Петр",
					MiddleName = "Анатольевич",
					LastName = "Иванов",
					BirthDate = new DateTime(1980, 5, 13),
					PolicyNumber = "5248697532",
					Address = "г. Балаково, Минская 5-10",
					Phone = "83514869552",
					CreatedAt = DateTime.Now
				},
				new Patient
				{
					Id = 2,
					FirstName = "Алексей",
					MiddleName = "Леонидович",
					LastName = "Сидоров",
					BirthDate = new DateTime(1986, 1, 10),
					PolicyNumber = "8566459227",
					Address = "г. Балаково, Ленина 14-6",
					Phone = "85677951257",
					CreatedAt = DateTime.Now
				},
				new Patient
				{
					Id = 3,
					FirstName = "Алексей",
					MiddleName = "Олегович",
					LastName = "Титаренко",
					BirthDate = new DateTime(1999, 6, 20),
					PolicyNumber = "5786249945",
					Address = "г. Балаково, Ленина 52-46",
					Phone = "85478562184",
					CreatedAt = DateTime.Now
				},
				new Patient
				{
					Id = 4,
					FirstName = "Петр",
					MiddleName = "Николаевич",
					LastName = "Смирнов",
					BirthDate = new DateTime(2000, 11, 25),
					PolicyNumber = "4586255789",
					Address = "г. Балаково, Красноармейская 13-39",
					Phone = "88453265874",
					CreatedAt = DateTime.Now
				},
				new Patient
				{
					Id = 5,
					FirstName = "Максим",
					MiddleName = "Сергеевич",
					LastName = "Титаренко",
					BirthDate = new DateTime(1975, 12, 2),
					PolicyNumber = "7521585248",
					Address = "г. Балаково, Трнавская 59-85",
					Phone = "89615842255",
					CreatedAt = DateTime.Now
				});
		}

		private void AddOrUpdateProcedures(DentalClinicContext context)
		{
			var proceduresSet = context.Set<Procedure>();

			proceduresSet.AddOrUpdate(x => x.Id,
				new Procedure
				{
					Id = 1,
					Name = "Нет",
					Price = 0M,
					MedicamentId = 1,
					CreatedAt = DateTime.Now
				},
				new Procedure
				{
					Id = 2,
					Name = "Компьютерная томография зубов",
					Price = 3900M,
					MedicamentId = 1,
					CreatedAt = DateTime.Now
				},
				new Procedure
				{
					Id = 3,
					Name = "Снятие зубных отложений с помощью ультразвука",
					Price = 2200M,
					MedicamentId = 2,
					CreatedAt = DateTime.Now
				},
				new Procedure
				{
					Id = 4,
					Name = "Чистка зубов от налета",
					Price = 500M,
					MedicamentId = 3,
					CreatedAt = DateTime.Now
				},
				new Procedure
				{
					Id = 5,
					Name = "Ортопантомограмма",
					Price = 800M,
					MedicamentId = 4,
					CreatedAt = DateTime.Now
				},
				new Procedure
				{
					Id = 6,
					Name = "Отбеливание зубов системой Opalescence",
					Price = 9900M,
					MedicamentId = 6,
					CreatedAt = DateTime.Now
				});
		}
	}
}