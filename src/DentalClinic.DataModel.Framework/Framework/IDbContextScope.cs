﻿using System;
using System.Data.Entity;

namespace DentalClinic.DataModel.Framework
{
	public interface IDbContextScope
	{
		DbContext GetContext();
	}
}