﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading;

using DentalClinic.DataModel.Mapping;

namespace DentalClinic.DataModel.Framework
{
	public class DentalClinicContext : DbContext
	{
		public DentalClinicContext() : base("name=DefaultConnection") { }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			var mappers = DentalClinicSchema.GetMappers();
			foreach (var mapper in mappers)
			{
				mapper.Map(modelBuilder);
			}
		}
	}
}