﻿using System;
using System.Data;
using System.Data.Entity;

namespace DentalClinic.DataModel.Framework
{
	public interface ITransactionScope : IDisposable
	{
		DbContext Context { get; }

		DbContextTransaction Transaction { get; }
	}
}