﻿using System;
using System.Collections.Generic;
using System.Data.Entity;

using DentalClinic.DataModel.Mapping;

namespace DentalClinic.DataModel.Framework
{
	public static class DentalClinicSchema
	{
		public static IEnumerable<IEntityMapper> GetMappers()
		{
			yield return new PatientMapper();
			yield return new DoctorMapper();
			yield return new AnalysisMapper();
			yield return new MedicamentMapper();
			yield return new DiagnosisMapper();
			yield return new AppointmentMapper();
			yield return new ProcedureMapper();
			yield return new TreatmentMapper();
			yield break;
		}
	}
}