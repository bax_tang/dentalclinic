﻿using System;

using NLog;

namespace DentalClinic.Models
{
	public abstract class ApplicationModelBase
	{
		protected readonly ILogger _logger;

		protected internal ApplicationModelBase(ILogger logger)
		{
			_logger = logger;
		}
	}
}