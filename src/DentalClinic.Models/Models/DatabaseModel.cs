﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

using NLog;

using DentalClinic.Services;

namespace DentalClinic.Models
{
	public class DatabaseModel : ApplicationModelBase
	{
		private readonly IDatabaseService _databaseService;

		public DatabaseModel(ILogger logger, IDatabaseService databaseService) : base(logger)
		{
			_databaseService = databaseService;
		}

		public async Task<bool> MigrateToLatestVersionAsync()
		{
			try
			{
				return await _databaseService.MigrateToLatestVersionAsync();
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Exception occured during database migration.");
				throw;
			}
		}
	}
}