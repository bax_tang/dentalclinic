﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using NLog;

using DentalClinic.Common;
using DentalClinic.Domain;
using DentalClinic.Domain.Results;
using DentalClinic.Services;

namespace DentalClinic.Models
{
	public class DentalClinicModel : ApplicationModelBase
	{
		private readonly IDentalClinicService _dentalClinicService;

		public ShortPatientModel SelectedPatient { get; set; }

		public event Action<ShortPatientModel> NewPatientAdded;

		public event Action<AppointmentModel> NewAppointmentAdded;

		public DentalClinicModel(ILogger logger, IDentalClinicService dentalClinicService) : base(logger)
		{
			_dentalClinicService = dentalClinicService;
		}

		public async Task<AppointmentModel> AddAppointmentAsync(ShortPatientModel patient, ShortDoctorModel doctor, DateTime appointmentDate)
		{
			try
			{
				var appointmentId = await _dentalClinicService.AddAppointmentAsync(patient.Id, doctor.Id, appointmentDate);

				var appointment = await _dentalClinicService.GetAppointmentAsync(appointmentId);

				NewAppointmentAdded?.Invoke(appointment);

				return appointment;
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось создать запись.");
				throw;
			}
		}

		public async Task<CollectionResult<AnalysisModel>> GetAnalyzesAsync()
		{
			try
			{
				return await _dentalClinicService.GetAnalyzesAsync();
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось загрузить список анализов.");
				throw;
			}
		}

		public async Task DeleteAnalysisAsync(AnalysisModel analysis)
		{
			try
			{
				await _dentalClinicService.DeleteAnalysisAsync(analysis.Id);
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось выполнить удаление вида анализа.");
				throw;
			}
		}

		public async Task UpdateAnalyzesAsync(List<AnalysisModel> analyzes)
		{
			try
			{
				await _dentalClinicService.UpdateAnalyzesAsync(analyzes);
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось выполнить сохранение списка анализов.");
				throw;
			}
		}

		public async Task<CollectionResult<DiagnosisModel>> GetDiagnosesAsync()
		{
			try
			{
				return await _dentalClinicService.GetDiagnosesAsync();
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось загрузить список диагнозов.");
				throw;
			}
		}

		public async Task DeleteDiagnosisAsync(DiagnosisModel diagnosis)
		{
			try
			{
				await _dentalClinicService.DeleteDiagnosisAsync(diagnosis.Id);
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось выполнить удаление диагноза.");
				throw;
			}
		}

		public async Task UpdateDiagnosesAsync(List<DiagnosisModel> diagnoses)
		{
			try
			{
				await _dentalClinicService.UpdateDiagnosesAsync(diagnoses);
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось выполнить сохранение списка диагнозов.");
				throw;
			}
		}

		public async Task<CollectionResult<AppointmentScheduleModel>> GetAppointmentsForScheduleAsync(long doctorId, DateTime date)
		{
			try
			{
				var appointments = await _dentalClinicService.GetAppointmentsForDoctorAsync(doctorId, date);

				var schedule = FillScheduleList(appointments, date);

				return new CollectionResult<AppointmentScheduleModel>(schedule)
				{
					PageNumber = 1,
					PagesCount = 1,
					PageSize = schedule.Count,
					TotalCount = schedule.Count
				};
			}
			catch (Exception exc)
			{
				_logger.Error(exc, $"Не удалось получить расписание для врача с номером '{doctorId}' на дату '{date}'.");
				throw;
			}
		}

		public async Task DeleteAppointmentAsync(AppointmentModel appointment)
		{
			try
			{
				await _dentalClinicService.DeleteAppointmentAsync(appointment.Id);
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось удалить запись.");
				throw;
			}
		}

		public async Task<ShortDoctorModel> GetDoctorAsync(long doctorId)
		{
			try
			{
				return await _dentalClinicService.GetDoctorAsync(doctorId);
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось получить информацию о враче.");
				throw;
			}
		}

		public async Task<CollectionResult<ShortDoctorModel>> GetDoctorsAsync()
		{
			try
			{
				return await _dentalClinicService.GetDoctorsAsync();
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось загрузить список докторов.");
				throw;
			}
		}

		public async Task<CollectionResult<FullDoctorModel>> GetDoctorsFullInfoAsync()
		{
			try
			{
				return await _dentalClinicService.GetDoctorsFullInfoAsync();
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось загрузить список врачей.");
				throw;
			}
		}

		public async Task<CollectionResult<ShortMedicamentModel>> GetMedicamentsInfoAsync()
		{
			try
			{
				return await _dentalClinicService.GetMedicamentsInfoAsync();
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось загрузить список медикаментов.");
				throw;
			}
		}

		public async Task<CollectionResult<MedicamentModel>> GetMedicamentsAsync()
		{
			try
			{
				return await _dentalClinicService.GetMedicamentsAsync();
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось загрузить список медикаментов.");
				throw;
			}
		}

		public async Task DeleteMedicamentAsync(MedicamentModel medicament)
		{
			try
			{
				await _dentalClinicService.DeleteMedicamentAsync(medicament.Id);
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось удалить медикамент.");
				throw;
			}
		}

		public async Task UpdateMedicamentsAsync(List<MedicamentModel> medicaments)
		{
			try
			{
				await _dentalClinicService.UpdateMedicamentsAsync(medicaments);
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось выполнить сохранение списка медикаментов.");
				throw;
			}
		}

		public async Task<CollectionResult<ProcedureModel>> GetProceduresAsync()
		{
			try
			{
				return await _dentalClinicService.GetProceduresAsync();
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось загрузить список процедур.");
				throw;
			}
		}

		public async Task DeleteProcedureAsync(ProcedureModel procedure)
		{
			try
			{
				await _dentalClinicService.DeleteProcedureAsync(procedure.Id);
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось удалить процедуру.");
				throw;
			}
		}

		public async Task UpdateProceduresAsync(List<ProcedureModel> procedures)
		{
			try
			{
				await _dentalClinicService.UpdateProceduresAsync(procedures);
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось обновить список процедур.");
				throw;
			}
		}

		public async Task DeleteDoctorAsync(long doctorId)
		{
			try
			{
				await _dentalClinicService.DeleteDoctorAsync(doctorId);
			}
			catch (Exception exc)
			{
				_logger.Error(exc, $"Не удалось удалить врача с идентификатором {doctorId}.");
				throw;
			}
		}

		public async Task UpdateDoctorsAsync(List<FullDoctorModel> doctors)
		{
			try
			{
				await _dentalClinicService.UpdateDoctorsAsync(doctors);
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось выполнить сохранение списка врачей.");
				throw;
			}
		}

		public async Task<CollectionResult<ShortPatientModel>> GetPatientsAsync(int pageNumber = 1, int pageSize = 10)
		{
			try
			{
				return await _dentalClinicService.GetPatientsAsync(pageNumber, pageSize);
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось загрузить список пациентов.");
				throw;
			}
		}

		public async Task<CollectionResult<ShortPatientModel>> SearchPatientsAsync(string searchText, int pageNumber = 1, int pageSize = 10)
		{
			try
			{
				return await _dentalClinicService.SearchPatientsAsync(searchText, pageNumber, pageSize);
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось выполнить поиск пациентов.");
				throw;
			}
		}

		public async Task<ShortPatientModel> GetShortPatientInfoAsync(long patientId)
		{
			try
			{
				return await _dentalClinicService.GetShortPatientInfoAsync(patientId);
			}
			catch (Exception exc)
			{
				_logger.Error(exc, $"Не удалось загрузить информацию о пациенте.");
				throw;
			}
		}

		public async Task<OperationResult<FullPatientModel>> GetFullPatientInfoAsync(long patientId, bool loadExtendedInfo = false)
		{
			try
			{
				return await _dentalClinicService.GetPatientAsync(patientId, loadExtendedInfo);
			}
			catch (Exception exc)
			{
				_logger.Error(exc, $"Не удалось загрузить информацию о пациенте.");
				throw;
			}
		}

		public async Task<long> SavePatientAsync(FullPatientModel patient)
		{
			try
			{
				bool isNew = patient.Id == -1;
				long patientId = await _dentalClinicService.SavePatientAsync(patient);

				if (isNew && patientId != -1)
				{
					NewPatientAdded?.Invoke(new ShortPatientModel
					{
						Id = patientId,
						LastName = patient.LastName,
						FirstName = patient.FirstName,
						MiddleName = patient.MiddleName,
						PolicyNumber = patient.PolicyNumber
					});
				}

				return patientId;
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось сохранить информацию о пациенте.");
				throw;
			}
		}

		public async Task<CollectionResult<TreatmentModel>> GetTreatmentsAsync(AppointmentModel appointment)
		{
			try
			{
				return await _dentalClinicService.GetTreatmentsAsync(appointment.Id);
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось загрузить информацию о назначениях.");
				throw;
			}
		}

		public async Task SaveTreatmentsAsync(AppointmentModel appointment, List<TreatmentModel> treatments)
		{
			try
			{
				await _dentalClinicService.SaveTreatmentsAsync(appointment.Id, treatments);
			}
			catch (Exception exc)
			{
				_logger.Error(exc, "Не удалось сохранить информацию о назначенном лечении.");
				throw;
			}
		}

		private List<AppointmentScheduleModel> FillScheduleList(List<AppointmentModel> appointments, DateTime date)
		{
			var scheduleList = new List<AppointmentScheduleModel>(14);

			DateTime scheduleDate = date.Date.AddHours(8.0);
			int index = 0;

			do
			{
				AppointmentModel appointment = null;

				if (index < appointments.Count)
				{
					while (appointments[index].Date <= scheduleDate)
					{
						appointment = appointments[index];

						if (++index >= appointments.Count) break;
					}
				}

				scheduleList.Add(new AppointmentScheduleModel
				{
					Appointment = appointment,
					AppointmentDate = scheduleDate
				});

				scheduleDate = scheduleDate.AddHours(1.0);
			}
			while (scheduleDate.Hour <= 20);

			return scheduleList;
		}
	}
}