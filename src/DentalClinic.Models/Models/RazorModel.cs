﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NLog;
using RazorEngine;
using RazorEngine.Templating;

using DentalClinic.Domain;

namespace DentalClinic.Models
{
	public class RazorModel : ApplicationModelBase
	{
		private ITemplateKey _orderTemplateKey;
		private ITemplateSource _orderTemplateSource;
		private ITemplateKey _scheduleTemplateKey;
		private ITemplateSource _scheduleTemplateSource;

		public RazorModel(ILogger logger) : base(logger) { }

		public string GenerateOrder(OrderModel order)
		{
			string filePath = Path.GetTempFileName();
			
			try
			{
				string htmlText = null;
				bool isFirstRun = (_orderTemplateKey == null);
				
				if (isFirstRun)
				{
					AddOrderTemplate();

					htmlText = Engine.Razor.RunCompile(_orderTemplateSource, _orderTemplateKey, model: order);
				}
				else
				{
					htmlText = Engine.Razor.Run(_orderTemplateKey, model: order);
				}

				File.WriteAllText(filePath, htmlText, System.Text.Encoding.UTF8);
			}
			catch (Exception exc)
			{
				try
				{
					File.Delete(filePath);
				}
				catch { }

				_logger.Error(exc, "Не удалось сформировать файл заказа.");
				throw;
			}

			return filePath;
		}

		public string GenerateSchedule(ScheduleModel schedule)
		{
			string filePath = Path.GetTempFileName();

			try
			{
				string htmlText = null;
				bool isFirstRun = (_scheduleTemplateKey == null);

				if (isFirstRun)
				{
					AddScheduleTemplate();

					htmlText = Engine.Razor.RunCompile(_scheduleTemplateSource, _scheduleTemplateKey, model: schedule);
				}
				else
				{
					htmlText = Engine.Razor.Run(_scheduleTemplateKey, model: schedule);
				}

				File.WriteAllText(filePath, htmlText, System.Text.Encoding.UTF8);
			}
			catch (Exception exc)
			{
				try
				{
					File.Delete(filePath);
				}
				catch { }

				_logger.Error(exc, "Не удалось сформировать файл расписания.");
				throw;
			}

			return filePath;
		}

		private void AddOrderTemplate()
		{
			string directory = AppDomain.CurrentDomain.BaseDirectory;
			string templatePath = Path.Combine(directory, @"Templates\OrderTemplate.cshtml");
			string templateName = "OrderTemplate";

			var key = new FullPathTemplateKey(templateName, templatePath, ResolveType.Global, null);

			var templateText = File.ReadAllText(templatePath, System.Text.Encoding.UTF8);
			var source = new LoadedTemplateSource(templateText, templatePath);

			Engine.Razor.AddTemplate(key, source);

			_orderTemplateKey = key;
			_orderTemplateSource = source;
		}

		private void AddScheduleTemplate()
		{
			string directory = AppDomain.CurrentDomain.BaseDirectory;
			string templatePath = Path.Combine(directory, @"Templates\ScheduleTemplate.cshtml");
			string templateName = "ScheduleTemplate";

			var key = new FullPathTemplateKey(templateName, templatePath, ResolveType.Global, null);

			var templateText = File.ReadAllText(templatePath, System.Text.Encoding.UTF8);
			var source = new LoadedTemplateSource(templateText, templatePath);

			Engine.Razor.AddTemplate(key, source);

			_scheduleTemplateKey = key;
			_scheduleTemplateSource = source;
		}
	}
}