﻿using System;

namespace DentalClinic.Domain
{
	public abstract class DomainObjectModel
	{
		public bool IsNew { get; set; }

		public bool IsChanged { get; set; }
	}
}