﻿using System;
using System.Collections.Generic;

namespace DentalClinic.Domain
{
	public class ScheduleModel
	{
		public DateTime Date { get; set; }

		public string DoctorFullName { get; set; }

		public IList<AppointmentScheduleModel> Appointments { get; set; }
	}
}