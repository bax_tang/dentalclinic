﻿using System;

namespace DentalClinic.Domain
{
	public class AppointmentModel
	{
		public long Id { get; set; }

		public DateTime Date { get; set; }

		public long DoctorId { get; set; }

		public string DoctorName { get; set; }

		public long PatientId { get; set; }

		public string PatientFirstName { get; set; }

		public string PatientLastName { get; set; }
		
		public string PatientFullName
		{
			get
			{
				return string.Concat(PatientLastName, " ", PatientFirstName);
			}
		}
	}
}