﻿using System;

namespace DentalClinic.Domain
{
	public class ProcedureModel : DomainObjectModel
	{
		private long? _medicamentId;

		public long Id { get; set; }

		public string Name { get; set; }

		public decimal Price { get; set; }

		public DateTime? CreatedAt { get; set; }

		public long? MedicamentId
		{
			get { return _medicamentId; }
			set
			{
				if (_medicamentId != value)
				{
					_medicamentId = value;
					IsChanged = true;
				}
			}
		}

		public string MedicamentName { get; set; }

		public string MedicamentDosage { get; set; }

		public decimal MedicamentPrice { get; set; }

		public override string ToString()
		{
			return Name;
		}
	}
}