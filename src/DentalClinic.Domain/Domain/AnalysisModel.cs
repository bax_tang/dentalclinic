﻿using System;

namespace DentalClinic.Domain
{
	public class AnalysisModel : DomainObjectModel
	{
		public long Id { get; set; }

		public string WorkKind { get; set; }

		public decimal Price { get; set; }

		public DateTime? CreatedAt { get; set; }

		public override string ToString()
		{
			return WorkKind;
		}
	}
}