﻿using System;

namespace DentalClinic.Domain
{
	public class DiagnosisModel : DomainObjectModel
	{
		public long Id { get; set; }

		public string Type { get; set; }

		public string Name { get; set; }

		public DateTime? Date { get; set; }

		public DateTime? CreatedAt { get; set; }

		public override string ToString()
		{
			return string.Format("{0} ({1})", Type, Name);
		}
	}
}