﻿using System;

namespace DentalClinic.Domain
{
	public class ShortMedicamentModel
	{
		public long Id { get; set; }

		public string Name { get; set; }

		public string Dosage { get; set; }

		public string FullName
		{
			get { return string.Format("{0} ({1})", Name, Dosage); }
		}

		public override string ToString()
		{
			return FullName;
		}
	}
}