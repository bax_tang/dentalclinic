﻿using System;

namespace DentalClinic.Domain
{
	public class ShortPatientModel
	{
		private string _fullName = null;

		public long Id { get; set; }

		public string FirstName { get; set; }

		public string MiddleName { get; set; }

		public string LastName { get; set; }

		public string FullName
		{
			get { return BuildFullName(); }
		}

		public string PolicyNumber { get; set; }

		public override string ToString()
		{
			return BuildFullName();
		}

		private string BuildFullName()
		{
			if (string.IsNullOrEmpty(_fullName))
			{
				string firstName = (FirstName?.Length > 0) ? " " + FirstName.Substring(0, 1) + "." : "";
				string middleName = (MiddleName?.Length > 0) ? " " + MiddleName.Substring(0, 1) + "." : "";

				_fullName = string.Concat(LastName, firstName, middleName);
			}

			return _fullName;
		}
	}
}