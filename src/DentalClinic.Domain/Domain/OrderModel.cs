﻿using System;
using System.Collections.Generic;

namespace DentalClinic.Domain
{
	public class OrderModel
	{
		public long Id { get; set; }

		public DateTime Date { get; set; }

		public string PatientFullName { get; set; }

		public string DoctorFullName { get; set; }

		public decimal TotalPrice { get; set; }

		public List<OrderItemModel> Items { get; set; }
	}

	public class OrderItemModel
	{
		public string ProcedureName { get; set; }

		public string MedicamentName { get; set; }

		public decimal Price { get; set; }
	}
}