﻿using System;

namespace DentalClinic.Domain
{
	public class TreatmentModel : PropertyChangedModel
	{
		private long _analysisId = -1;
		private long _diagnosisId = -1;
		private long _procedureId = -1;
		private string _comments;
		private decimal _totalPrice = -1M;

		public long Id { get; set; }
		
		public DateTime? Date { get; set; }

		public long AnalysisId
		{
			get { return _analysisId; }
			set
			{
				_analysisId = value;
				RaisePropertyChanged();
			}
		}

		public string AnalysisName { get; set; }

		public decimal AnalysisPrice { get; set; }

		public long DiagnosisId
		{
			get { return _diagnosisId; }
			set
			{
				_diagnosisId = value;
				RaisePropertyChanged();
			}
		}

		public long PatientId { get; set; }

		public string PatientName { get; set; }

		public long DoctorId { get; set; }

		public string DoctorName { get; set; }

		public long MedicamentId { get; set; }

		public string MedicamentName { get; set; }

		public decimal MedicamentPrice { get; set; }

		public long ProcedureId
		{
			get { return _procedureId; }
			set
			{
				_procedureId = value;
				RaisePropertyChanged();
			}
		}

		public string ProcedureName { get; set; }

		public decimal ProcedurePrice { get; set; }

		public string Comments
		{
			get { return _comments; }
			set
			{
				_comments = value;
				RaisePropertyChanged();
			}
		}

		public decimal TotalPrice
		{
			get
			{
				if (_totalPrice == -1M)
				{
					_totalPrice = AnalysisPrice + ProcedurePrice + MedicamentPrice;
				}
				return _totalPrice;
			}
			set
			{
				_totalPrice = value;
				RaisePropertyChanged();
			}
		}

		public DateTime? CreatedAt { get; set; }

		public void UpdateTotalPrice()
		{
			TotalPrice = AnalysisPrice + MedicamentPrice + ProcedurePrice;
		}
	}
}