﻿using System;

namespace DentalClinic.Domain
{
	public class FullDoctorModel : DomainObjectModel
	{
		public long Id { get; set; }

		public string FirstName { get; set; }

		public string MiddleName { get; set; }

		public string LastName { get; set; }

		public string Category { get; set; }

		public int Seniority { get; set; }

		public DateTime? CreatedAt { get; set; }
	}
}