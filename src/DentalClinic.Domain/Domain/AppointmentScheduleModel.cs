﻿using System;

namespace DentalClinic.Domain
{
	public class AppointmentScheduleModel
	{
		public DateTime AppointmentDate { get; set; }

		public bool AppointmentExists
		{
			get { return Appointment != null; }
		}

		public bool AppointmentNotExists
		{
			get { return !AppointmentExists; }
		}

		public AppointmentModel Appointment { get; set; }
	}
}