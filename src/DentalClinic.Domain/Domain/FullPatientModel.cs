﻿using System;
using System.Collections.Generic;

namespace DentalClinic.Domain
{
	public class FullPatientModel
	{
		public long Id { get; set; }

		public string FirstName { get; set; }

		public string MiddleName { get; set; }

		public string LastName { get; set; }

		public DateTime BirthDate { get; set; }

		public string PolicyNumber { get; set; }

		public string PhoneNumber { get; set; }

		public string Address { get; set; }

		public DateTime? CreatedAt { get; set; }

		public IList<AppointmentModel> Appointments { get; set; }

		public IList<DiagnosisModel> Diagnoses { get; set; }

		public IList<TreatmentModel> Treatments { get; set; }
	}
}