﻿using System;

namespace DentalClinic.Domain
{
	public class MedicamentModel : DomainObjectModel
	{
		public long Id { get; set; }

		public string Name { get; set; }

		public string Dosage { get; set; }

		public decimal Price { get; set; }

		public DateTime? CreatedAt { get; set; }
	}
}