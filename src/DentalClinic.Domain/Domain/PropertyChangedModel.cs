﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DentalClinic.Domain
{
	public abstract class PropertyChangedModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		protected internal PropertyChangedModel() { }

		protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}