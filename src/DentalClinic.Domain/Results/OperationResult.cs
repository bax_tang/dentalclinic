﻿using System;

namespace DentalClinic.Domain.Results
{
	public class OperationResult<TModel>
	{
		public TModel Data { get; private set; }

		public Exception Error { get; private set; }

		public string ErrorMessage { get; private set; }

		public bool Succeeded { get; private set; }
		
		public OperationResult(TModel data)
		{
			Data = data;
			Succeeded = true;
		}

		public OperationResult(string errorMessage, Exception error = null)
		{
			ErrorMessage = errorMessage;
			Error = error ?? new Exception(errorMessage);
			Succeeded = false;
		}
	}
}