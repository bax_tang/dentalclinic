﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DentalClinic.Domain.Results
{
	public class CollectionResult<TItem> : IEnumerable<TItem>
	{
		public int PageNumber { get; set; }

		public int PageSize { get; set; }

		public int PagesCount { get; set; }

		public int TotalCount { get; set; }

		public IEnumerable<TItem> Items { get; private set; }

		public IEnumerator<TItem> GetEnumerator() => Items.GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() => Items.GetEnumerator();

		public CollectionResult(IEnumerable<TItem> items)
		{
			Items = items;
		}
	}
}