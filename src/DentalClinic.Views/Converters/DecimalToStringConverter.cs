﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DentalClinic.Views.Converters
{
	public class DecimalToStringConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is decimal)
			{
				return string.Format(CultureInfo.InvariantCulture, "{0:N}", value);
			}

			return value.ToString();
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			decimal decimalValue = 0M;

			if (value is string)
			{
				decimal.TryParse((string)value, NumberStyles.Any, CultureInfo.InvariantCulture, out decimalValue);
			}

			return decimalValue;
		}
	}
}