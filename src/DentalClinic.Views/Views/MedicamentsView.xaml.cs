﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Mvvm;

using MahApps.Metro.Controls;

using DentalClinic.ViewModels;

namespace DentalClinic.Views
{
	public partial class MedicamentsView : MetroWindow, IModalView
	{
		private MedicamentsViewModel ViewModel
		{
			get { return DataContext as MedicamentsViewModel; }
		}

		public MedicamentsView() : base()
		{
			InitializeComponent();

			Loaded += MedicamentsViewLoaded;
			Unloaded += MedicamentsViewUnloaded;
		}

		private void MedicamentsViewLoaded(object sender, RoutedEventArgs args)
		{
			Loaded -= MedicamentsViewLoaded;

			var viewModel = ViewModel;
			if (viewModel != null)
			{
				viewModel.NewItemAdded += OnNewItemAdded;
			}
		}

		private void MedicamentsViewUnloaded(object sender, RoutedEventArgs args)
		{
			Unloaded -= MedicamentsViewUnloaded;

			var viewModel = ViewModel;
			if (viewModel != null)
			{
				viewModel.NewItemAdded -= OnNewItemAdded;
			}
		}

		private void OnNewItemAdded()
		{
			medicamentsGrid.ScrollIntoView(ViewModel.CurrentItem, medicamentsGrid.Columns[1]);
			medicamentsGrid.BeginEdit();
		}

		private void MedicamentsGridCellEditEnding(object sender, DataGridCellEditEndingEventArgs args)
		{
			if (args.EditAction == DataGridEditAction.Commit)
			{
				ViewModel?.HandleEditEnding(args.Row.Item);
			}
		}

		private void MedicamentsGridPreparingCellForEdit(object sender, DataGridPreparingCellForEditEventArgs args)
		{
			if (args.EditingElement.Focusable && !args.EditingElement.IsFocused)
			{
				args.EditingElement.Focus();
			}
		}

		private void MedicamentsGridRowEditEnding(object sender, DataGridRowEditEndingEventArgs args)
		{
			if (args.EditAction == DataGridEditAction.Commit)
			{
				ViewModel?.HandleEditEnding(args.Row.Item);
			}
		}

		private async void HandleClosingWindow(object sender, ClosingWindowEventHandlerArgs args)
		{
			var viewModel = ViewModel;
			if (viewModel != null && viewModel.HasChanges)
			{
				args.Cancelled = true;

				await viewModel.HandleClosingAsync();

				Close();
			}
		}
	}
}