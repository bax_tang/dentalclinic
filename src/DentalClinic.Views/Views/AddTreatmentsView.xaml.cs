﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Mvvm;

using MahApps.Metro.Controls;

using DentalClinic.Domain;
using DentalClinic.ViewModels;

namespace DentalClinic.Views
{
	public partial class AddTreatmentsView : MetroWindow, IModalView
	{
		private AddTreatmentsViewModel ViewModel
		{
			get { return DataContext as AddTreatmentsViewModel; }
		}

		public AddTreatmentsView() : base()
		{
			InitializeComponent();
		}

		private void AnalyzesComboBoxSelectionChanged(object sender, SelectionChangedEventArgs args)
		{
			var viewModel = ViewModel;
			if (args.AddedItems.Count > 0 && viewModel != null)
			{
				var analysisModel = args.AddedItems[0] as AnalysisModel;

				viewModel.HandleAnalysisSelectionChanging(analysisModel);
			}
		}

		private void ProceduresComboBoxSelectionChanged(object sender, SelectionChangedEventArgs args)
		{
			var viewModel = ViewModel;
			if (args.AddedItems.Count > 0 && viewModel != null)
			{
				var procedureModel = args.AddedItems[0] as ProcedureModel;

				viewModel.HandleProcedureSelectionChanging(procedureModel);
			}
		}
	}
}