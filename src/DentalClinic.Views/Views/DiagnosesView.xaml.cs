﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Mvvm;

using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

using DentalClinic.ViewModels;

namespace DentalClinic.Views
{
	public partial class DiagnosesView : MetroWindow, IModalView
	{
		private DiagnosesViewModel ViewModel
		{
			get { return DataContext as DiagnosesViewModel; }
		}

		public DiagnosesView() : base()
		{
			InitializeComponent();

			Loaded += DiagnosesViewLoaded;
			Unloaded += DiagnosesViewUnloaded;
		}

		private void DiagnosesViewLoaded(object sender, RoutedEventArgs args)
		{
			Loaded -= DiagnosesViewLoaded;

			var viewModel = ViewModel;
			if (viewModel != null)
			{
				viewModel.NewItemAdded += OnNewItemAdded;
			}
		}

		private void DiagnosesViewUnloaded(object sender, RoutedEventArgs args)
		{
			Unloaded -= DiagnosesViewUnloaded;

			var viewModel = ViewModel;
			if (viewModel != null)
			{
				viewModel.NewItemAdded -= OnNewItemAdded;
			}
		}

		private void OnNewItemAdded()
		{
			diagnosesGrid.ScrollIntoView(ViewModel.CurrentItem, diagnosesGrid.Columns[1]);
			diagnosesGrid.BeginEdit();
		}

		private void DiagnosesGridPreparingCellForEdit(object sender, DataGridPreparingCellForEditEventArgs args)
		{
			if (args.EditingElement.Focusable && !args.EditingElement.IsFocused)
			{
				args.EditingElement.Focus();
			}
		}

		private void DiagnosesGridCellEditEnding(object sender, DataGridCellEditEndingEventArgs args)
		{
			if (args.EditAction == DataGridEditAction.Commit)
			{
				ViewModel?.HandleEditEnding(args.Row.Item);
			}
		}

		private void DiagnosesGridRowEditEnding(object sender, DataGridRowEditEndingEventArgs args)
		{
			if (args.EditAction == DataGridEditAction.Commit)
			{
				ViewModel?.HandleEditEnding(args.Row.Item);
			}
		}

		private async void HandleClosingWindow(object sender, ClosingWindowEventHandlerArgs args)
		{
			var viewModel = ViewModel;
			if (viewModel != null && viewModel.HasChanges)
			{
				args.Cancelled = true;

				await viewModel.HandleClosingAsync();

				Close();
			}
		}
	}
}