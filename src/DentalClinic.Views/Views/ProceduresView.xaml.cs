﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Mvvm;

using MahApps.Metro.Controls;

using DentalClinic.ViewModels;

namespace DentalClinic.Views
{
	public partial class ProceduresView : MetroWindow, IModalView
	{
		private ProceduresViewModel ViewModel
		{
			get { return DataContext as ProceduresViewModel; }
		}

		public ProceduresView() : base()
		{
			InitializeComponent();

			Loaded += ProceduresViewLoaded;
			Unloaded += ProceduresViewUnloaded;
		}

		private void ProceduresViewLoaded(object sender, RoutedEventArgs args)
		{
			Loaded -= ProceduresViewLoaded;

			var viewModel = ViewModel;
			if (viewModel != null)
			{
				viewModel.NewItemAdded += OnNewItemAdded;
			}
		}

		private void ProceduresViewUnloaded(object sender, RoutedEventArgs args)
		{
			Unloaded -= ProceduresViewUnloaded;

			var viewModel = ViewModel;
			if (viewModel != null)
			{
				viewModel.NewItemAdded -= OnNewItemAdded;
			}
		}

		private void OnNewItemAdded()
		{
			proceduresGrid.ScrollIntoView(ViewModel.CurrentItem, proceduresGrid.Columns[1]);
			proceduresGrid.BeginEdit();
		}

		private void ProceduresGridCellEditEnding(object sender, DataGridCellEditEndingEventArgs args)
		{
			if (args.EditAction == DataGridEditAction.Commit)
			{
				ViewModel?.HandleEditEnding(args.Row.Item);
			}
		}

		private void ProceduresGridPreparingCellForEdit(object sender, DataGridPreparingCellForEditEventArgs args)
		{
			if (args.EditingElement.Focusable && !args.EditingElement.IsFocused)
			{
				args.EditingElement.Focus();
			}
		}

		private void ProceduresGridRowEditEnding(object sender, DataGridRowEditEndingEventArgs args)
		{
			if (args.EditAction == DataGridEditAction.Commit)
			{
				ViewModel?.HandleEditEnding(args.Row.Item);
			}
		}

		private async void HandleClosingWindow(object sender, ClosingWindowEventHandlerArgs args)
		{
			var viewModel = ViewModel;
			if (viewModel != null && viewModel.HasChanges)
			{
				args.Cancelled = true;

				await viewModel.HandleClosingAsync();

				Close();
			}
		}

		private void MedicamentComboBoxSelectionChanged(object sender, SelectionChangedEventArgs args)
		{
			if (args.AddedItems.Count > 0 && args.RemovedItems.Count > 0)
			{
				var viewModel = ViewModel;
				if (viewModel != null)
				{
					viewModel.HandleEditEnding(viewModel.CurrentItem);
				}
			}
		}
	}
}