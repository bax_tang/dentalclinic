﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Mvvm;

using MahApps.Metro.Controls;

using DentalClinic.ViewModels;

namespace DentalClinic.Views
{
	public partial class DoctorsView : MetroWindow, IModalView
	{
		private DoctorsViewModel ViewModel
		{
			get { return DataContext as DoctorsViewModel; }
		}

		public DoctorsView() : base()
		{
			InitializeComponent();

			Loaded += DoctorsViewLoaded;
			Unloaded += DoctorsViewUnloaded;
		}

		private void DoctorsViewLoaded(object sender, RoutedEventArgs args)
		{
			Loaded -= DoctorsViewLoaded;

			var viewModel = ViewModel;
			if (viewModel != null)
			{
				viewModel.NewItemAdded += OnNewItemAdded;
			}
		}

		private void DoctorsViewUnloaded(object sender, RoutedEventArgs args)
		{
			Unloaded -= DoctorsViewUnloaded;

			var viewModel = ViewModel;
			if (viewModel != null)
			{
				viewModel.NewItemAdded -= OnNewItemAdded;
			}
		}

		private void OnNewItemAdded()
		{
			doctorsGrid.ScrollIntoView(ViewModel.CurrentItem, doctorsGrid.Columns[1]);
			doctorsGrid.BeginEdit();
		}

		private void DoctorsGridPreparingCellForEdit(object sender, DataGridPreparingCellForEditEventArgs args)
		{
			if (args.EditingElement.Focusable && !args.EditingElement.IsFocused)
			{
				args.EditingElement.Focus();
			}
		}

		private void DoctorsGridCellEditEnding(object sender, DataGridCellEditEndingEventArgs args)
		{
			if (args.EditAction == DataGridEditAction.Commit)
			{
				ViewModel?.HandleEditEnding(args.Row.Item);
			}
		}

		private void DoctorsGridRowEditEnding(object sender, DataGridRowEditEndingEventArgs args)
		{
			if (args.EditAction == DataGridEditAction.Commit)
			{
				ViewModel?.HandleEditEnding(args.Row.Item);
			}
		}

		private async void HandleClosingWindow(object sender, ClosingWindowEventHandlerArgs args)
		{
			var viewModel = ViewModel;
			if (viewModel != null && viewModel.HasChanges)
			{
				args.Cancelled = true;

				await viewModel.HandleClosingAsync();

				Close();
			}
		}
	}
}