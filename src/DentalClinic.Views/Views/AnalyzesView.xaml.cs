﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using System.Windows.Mvvm;

using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

using DentalClinic.ViewModels;

namespace DentalClinic.Views
{
	public partial class AnalyzesView : MetroWindow, IModalView
	{
		private AnalyzesViewModel ViewModel
		{
			get { return DataContext as AnalyzesViewModel; }
		}

		public AnalyzesView() : base()
		{
			InitializeComponent();

			Loaded += AnalyzesViewLoaded;
			Unloaded += AnalyzesViewUnloaded;
		}

		private void AnalyzesViewLoaded(object sender, RoutedEventArgs args)
		{
			Loaded -= AnalyzesViewLoaded;

			var viewModel = ViewModel;
			if (viewModel != null)
			{
				viewModel.NewItemAdded += OnNewItemAdded;
			}
		}

		private void AnalyzesViewUnloaded(object sender, RoutedEventArgs args)
		{
			Unloaded -= AnalyzesViewUnloaded;

			var viewModel = ViewModel;
			if (viewModel != null)
			{
				viewModel.NewItemAdded -= OnNewItemAdded;
			}
		}

		private void OnNewItemAdded()
		{
			analyzesGrid.ScrollIntoView(ViewModel.CurrentItem, analyzesGrid.Columns[1]);
			analyzesGrid.BeginEdit();
		}

		private void AnalyzesGridPreparingCellForEdit(object sender, DataGridPreparingCellForEditEventArgs args)
		{
			if (args.EditingElement.Focusable && !args.EditingElement.IsFocused)
			{
				args.EditingElement.Focus();
			}
		}

		private void AnalyzesGridCellEditEnding(object sender, DataGridCellEditEndingEventArgs args)
		{
			if (args.EditAction == DataGridEditAction.Commit)
			{
				ViewModel?.HandleEditEnding(args.Row.Item);
			}
		}

		private void AnalyzesGridRowEditEnding(object sender, DataGridRowEditEndingEventArgs args)
		{
			if (args.EditAction == DataGridEditAction.Commit)
			{
				ViewModel?.HandleEditEnding(args.Row.Item);
			}
		}

		private async void HandleClosingWindow(object sender, ClosingWindowEventHandlerArgs args)
		{
			var viewModel = ViewModel;
			if (viewModel != null && viewModel.HasChanges)
			{
				args.Cancelled = true;

				await viewModel.HandleClosingAsync();

				Close();
			}
		}
	}
}