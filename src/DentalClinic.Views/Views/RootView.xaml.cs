﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Mvvm;

namespace DentalClinic.Views
{
	public partial class RootView : Page, IView
	{
		public RootView()
		{
			InitializeComponent();
		}
	}
}