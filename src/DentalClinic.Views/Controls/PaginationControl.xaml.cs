﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace DentalClinic.Views.Controls
{
	public partial class PaginationControl : UserControl
	{
		private const string PageTextFormat = "{0} / {1}";

		public static readonly DependencyProperty CurrentPageProperty;
		public static readonly DependencyProperty TotalPagesProperty;
		public static readonly DependencyProperty PageTextProperty;
		public static readonly DependencyProperty CanNavigateLeftProperty;
		public static readonly DependencyProperty CanNavigateRightProperty;
		public static readonly DependencyProperty GotoFirstPageCommandProperty;
		public static readonly DependencyProperty GotoPreviousPageCommandProperty;
		public static readonly DependencyProperty GotoNextPageCommandProperty;
		public static readonly DependencyProperty GotoLastPageCommandProperty;

		public int CurrentPage
		{
			get { return (int)GetValue(CurrentPageProperty); }
			set { SetValue(CurrentPageProperty, value); }
		}

		public int TotalPages
		{
			get { return (int)GetValue(TotalPagesProperty); }
			set { SetValue(TotalPagesProperty, value); }
		}

		public string PageText
		{
			get { return GetValue(PageTextProperty) as string; }
			set { SetValue(PageTextProperty, value); }
		}

		public bool CanNavigateLeft
		{
			get { return (bool)GetValue(CanNavigateLeftProperty); }
			private set
			{
				SetValue(CanNavigateLeftProperty, value);
			}
		}

		public bool CanNavigateRight
		{
			get { return (bool)GetValue(CanNavigateRightProperty); }
			private set
			{
				SetValue(CanNavigateRightProperty, value);
			}
		}

		public ICommand GotoFirstPageCommand
		{
			get { return (ICommand)GetValue(GotoFirstPageCommandProperty); }
			set { SetValue(GotoFirstPageCommandProperty, value); }
		}

		public ICommand GotoPreviousPageCommand
		{
			get { return (ICommand)GetValue(GotoPreviousPageCommandProperty); }
			set { SetValue(GotoPreviousPageCommandProperty, value); }
		}

		public ICommand GotoNextPageCommand
		{
			get { return (ICommand)GetValue(GotoNextPageCommandProperty); }
			set { SetValue(GotoNextPageCommandProperty, value); }
		}

		public ICommand GotoLastPageCommand
		{
			get { return (ICommand)GetValue(GotoLastPageCommandProperty); }
			set { SetValue(GotoLastPageCommandProperty, value); }
		}

		static PaginationControl()
		{
			CurrentPageProperty = DependencyProperty.Register(nameof(CurrentPage), typeof(int), typeof(PaginationControl),
				new PropertyMetadata(-1, PageInfoPropertiesChanged));

			TotalPagesProperty = DependencyProperty.Register(nameof(TotalPages), typeof(int), typeof(PaginationControl),
				new PropertyMetadata(-1, PageInfoPropertiesChanged));

			PageTextProperty = DependencyProperty.Register(nameof(PageText), typeof(string), typeof(PaginationControl));

			CanNavigateLeftProperty = DependencyProperty.Register(nameof(CanNavigateLeft), typeof(bool), typeof(PaginationControl));
			CanNavigateRightProperty = DependencyProperty.Register(nameof(CanNavigateRight), typeof(bool), typeof(PaginationControl));

			GotoFirstPageCommandProperty = DependencyProperty.Register(nameof(GotoFirstPageCommand), typeof(ICommand), typeof(PaginationControl));

			GotoPreviousPageCommandProperty = DependencyProperty.Register(nameof(GotoPreviousPageCommand), typeof(ICommand), typeof(PaginationControl));

			GotoNextPageCommandProperty = DependencyProperty.Register(nameof(GotoNextPageCommand), typeof(ICommand), typeof(PaginationControl));

			GotoLastPageCommandProperty = DependencyProperty.Register(nameof(GotoLastPageCommand), typeof(ICommand), typeof(PaginationControl));
		}

		public PaginationControl()
		{
			InitializeComponent();
		}

		private static void PageInfoPropertiesChanged(DependencyObject d, DependencyPropertyChangedEventArgs args)
		{
            var sender = d as PaginationControl;
			if (sender != null)
			{
				string pageText = string.Format(PageTextFormat, sender.CurrentPage, sender.TotalPages);

				sender.SetValue(PageTextProperty, pageText);

				bool canNavigateLeft = 1 < sender.CurrentPage,
					canNavigateRight = sender.CurrentPage < sender.TotalPages;

				sender.CanNavigateLeft = canNavigateLeft;
				sender.CanNavigateRight = canNavigateRight;
			}
		}
	}
}