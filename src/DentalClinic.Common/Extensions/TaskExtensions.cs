﻿using System;
using System.Threading.Tasks;

using NLog;

namespace DentalClinic.Common
{
	public static class TaskExtensions
	{
		private static readonly ILogger _logger;

		static TaskExtensions()
		{
			_logger = LogManager.GetCurrentClassLogger();
		}

		public static void DontAwaitButCheckExceptions(this Task task)
		{
			task.ContinueWith(HandleFaultedTask, TaskContinuationOptions.OnlyOnFaulted);
		}

		private static void HandleFaultedTask(Task faultedTask)
		{
			if (faultedTask.IsFaulted)
			{
				var exception = faultedTask.Exception.GetBaseException();

				_logger.Error(exception, "Task faulted with exception.");

#if DEBUG
				throw exception;
#endif
			}
		}
	}
}