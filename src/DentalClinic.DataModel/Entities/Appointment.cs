﻿using System;

namespace DentalClinic.DataModel.Entities
{
	/// <summary>
	/// Запись
	/// </summary>
	public class Appointment : UpdatableEntity
	{
		public DateTime Date { get; set; }

		public long DoctorId { get; set; }

		public long PatientId { get; set; }

		public virtual Doctor Doctor { get; set; }

		public virtual Patient Patient { get; set; }
	}
}