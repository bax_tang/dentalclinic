﻿using System;

namespace DentalClinic.DataModel.Entities
{
	/// <summary>
	/// Пациент
	/// </summary>
	public class Patient : UpdatableEntity
	{
		public string FirstName { get; set; }

		public string MiddleName { get; set; }

		public string LastName { get; set; }

		public DateTime BirthDate { get; set; }

		public string Address { get; set; }

		/// <summary>
		/// Номер страхового полиса
		/// </summary>
		public string PolicyNumber { get; set; }

		public string Phone { get; set; }
	}
}