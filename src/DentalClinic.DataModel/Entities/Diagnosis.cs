﻿using System;

namespace DentalClinic.DataModel.Entities
{
	/// <summary>
	/// Диагноз
	/// </summary>
	public class Diagnosis : UpdatableEntity
	{
		public string Type { get; set; }

		public string Name { get; set; }
	}
}