﻿using System;

namespace DentalClinic.DataModel.Entities
{
	/// <summary>
	/// Препарат
	/// </summary>
	public class Medicament : UpdatableEntity
	{
		public string Name { get; set; }

		/// <summary>
		/// Доза препарата
		/// </summary>
		public string Dosage { get; set; }

		public decimal Price { get; set; }
	}
}