﻿using System;

namespace DentalClinic.DataModel.Entities
{
	/// <summary>
	/// Врач
	/// </summary>
	public class Doctor : UpdatableEntity
	{
		public string FirstName { get; set; }

		public string MiddleName { get; set; }

		public string LastName { get; set; }

		public string Category { get; set; }

		/// <summary>
		/// Стаж работы
		/// </summary>
		public int Seniority { get; set; }
	}
}