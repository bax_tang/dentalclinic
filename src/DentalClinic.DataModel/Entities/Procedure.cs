﻿using System;

namespace DentalClinic.DataModel.Entities
{
	/// <summary>
	/// Процедура
	/// </summary>
	public class Procedure : UpdatableEntity
	{
		public string Name { get; set; }

		public decimal Price { get; set; }

		public long? MedicamentId { get; set; }

		public virtual Medicament Medicament { get; set; }
	}
}