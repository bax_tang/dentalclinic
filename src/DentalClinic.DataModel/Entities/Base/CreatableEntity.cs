﻿using System;

namespace DentalClinic.DataModel.Entities
{
	public abstract class CreatableEntity : BaseEntity
	{
		public DateTime? CreatedAt { get; set; }
	}
}