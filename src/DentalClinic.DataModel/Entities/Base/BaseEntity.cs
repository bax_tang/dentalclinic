﻿using System;

namespace DentalClinic.DataModel.Entities
{
	public abstract class BaseEntity
	{
		public long Id { get; set; }
	}
}