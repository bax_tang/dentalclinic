﻿using System;

namespace DentalClinic.DataModel.Entities
{
	public abstract class UpdatableEntity : CreatableEntity
	{
		public DateTime? UpdatedAt { get; set; }
	}
}