﻿using System;

namespace DentalClinic.DataModel.Entities
{
	/// <summary>
	/// Лечение
	/// </summary>
	public class Treatment : CreatableEntity
	{
		public long AppointmentId { get; set; }

		public long AnalysisId { get; set; }

		public long DiagnosisId { get; set; }

		public long ProcedureId { get; set; }

		public string Comments { get; set; }

		public virtual Appointment Appointment { get; set; }

		public virtual Analysis Analysis { get; set; }

		public virtual Diagnosis Diagnosis { get; set; }

		public virtual Procedure Procedure { get; set; }
	}
}