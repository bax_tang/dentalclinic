﻿using System;

namespace DentalClinic.DataModel.Entities
{
	/// <summary>
	/// Анализ
	/// </summary>
	public class Analysis : UpdatableEntity
	{
		/// <summary>
		/// Вид лабораторных работ
		/// </summary>
		public string WorkKind { get; set; }

		public decimal Price { get; set; }
	}
}