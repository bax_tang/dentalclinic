﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("DentalClinic")]
[assembly: AssemblyDescription("DentalClinic data model")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("DentalClinic")]
[assembly: AssemblyProduct("DentalClinic service application")]
[assembly: AssemblyCopyright("Copyright © DentalClinic, 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("edb433bb-e5d6-4f4a-83bb-403a1b33877a")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("0.2.0.*")]
[assembly: AssemblyFileVersion("0.2.0.0")]
