﻿using System;
using System.Windows.Mvvm;
using System.Windows.Navigation;

using MahApps.Metro.Controls.Dialogs;

namespace DentalClinic.ViewModels
{
	public interface INavigator
	{
		IDialogCoordinator DialogCoordinator { get; }

		INavigationService Navigation { get; }
	}
}