﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Mvvm;

using MahApps.Metro.Controls.Dialogs;

namespace DentalClinic.ViewModels
{
	public static class DialogExtensions
	{
		public static Task<MessageDialogResult> ShowExceptionDialog(this IDialogCoordinator coordinator, IViewAware viewAware, Exception error)
		{
			return coordinator.ShowMessageAsync(viewAware, "Возникло исключение", error.Message);
		}

		public static Task<MessageDialogResult> ShowQuestionAsync(this IDialogCoordinator coordinator, IViewAware viewAware, string question)
		{
			return coordinator.ShowMessageAsync(viewAware,
				"Требуется подтверждение",
				question,
				MessageDialogStyle.AffirmativeAndNegative,
				new MetroDialogSettings
				{
					AffirmativeButtonText = "Да",
					NegativeButtonText = "Нет",
					DefaultButtonFocus = MessageDialogResult.Affirmative,
					DialogResultOnCancel = MessageDialogResult.Negative
				});
		}

		public static Task<MessageDialogResult> ShowSaveChangesDialogAsync(this IDialogCoordinator coordinator, IViewAware viewAware)
		{
			return coordinator.ShowMessageAsync(viewAware,
				"Имеются несохранённые изменения",
				"Вы хотите сохранить их перед закрытием?",
				MessageDialogStyle.AffirmativeAndNegative,
				new MetroDialogSettings
				{
					AffirmativeButtonText = "Да",
					NegativeButtonText = "Нет",
					DefaultButtonFocus = MessageDialogResult.Affirmative,
					DialogResultOnCancel = MessageDialogResult.Negative
				});
		}
	}
}