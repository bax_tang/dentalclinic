﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Mvvm;

using MahApps.Metro.Controls.Dialogs;

using DentalClinic.Domain;
using DentalClinic.Models;
using DentalClinic.Domain.Results;

namespace DentalClinic.ViewModels
{
	public class AnalyzesViewModel : ReferenceEditorViewModel<AnalysisModel>
	{
		public AnalyzesViewModel(INavigator navigator, DentalClinicModel dentalClinicModel) : base(navigator, dentalClinicModel)
		{
		}

		protected override AnalysisModel CreateNew()
		{
			var newItem = new AnalysisModel
			{
				Id = -1,
				IsChanged = false,
				IsNew = true
			};
			return newItem;
		}

		protected override async Task<CollectionResult<AnalysisModel>> GetItemsCoreAsync()
		{
			return await _dentalClinicModel.GetAnalyzesAsync();
		}

		protected override async Task DeleteItemCoreAsync(AnalysisModel item)
		{
			try
			{
				if (item.Id != -1)
				{
					var message = $"Вы действительно хотите удалить \"{item.Id}: {item.WorkKind}\"?";
					var result = await _navigator.DialogCoordinator.ShowQuestionAsync(this, message);
					if (result == MessageDialogResult.Affirmative)
					{
						await _dentalClinicModel.DeleteAnalysisAsync(item);

						Items.Remove(item);
					}
				}
				else
					Items.Remove(item);
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
		}
		
		protected override async Task SaveChangesCoreAsync()
		{
			try
			{
				List<AnalysisModel> changedItems = GetChangedItems();

				await _dentalClinicModel.UpdateAnalyzesAsync(changedItems);
				await LoadInfoAsync();

				HasChanges = false;
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
		}
	}
}