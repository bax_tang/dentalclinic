﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Mvvm;

using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

using DentalClinic.Domain;
using DentalClinic.Domain.Results;
using DentalClinic.Models;

namespace DentalClinic.ViewModels
{
	public abstract class ReferenceEditorViewModel<TItem> : DentalClinicModalViewModel where TItem : DomainObjectModel
	{
		protected readonly DentalClinicModel _dentalClinicModel;

		private ObservableCollection<TItem> _items;
		private TItem _currentItem;
		private bool _hasChanges;

		public event Action NewItemAdded;

		public ObservableCollection<TItem> Items
		{
			get { return _items; }
		}

		public TItem CurrentItem
		{
			get { return _currentItem; }
			set
			{
				_currentItem = value;
				RaisePropertyChanged();
			}
		}

		public bool HasChanges
		{
			get { return _hasChanges; }
			protected set
			{
				if (_hasChanges != value)
				{
					_hasChanges = value;
					RaisePropertyChanged();
				}
			}
		}

		public ICommand RefreshCommand { get; }

		public ICommand AddNewCommand { get; }

		public ICommand DeleteCommand { get; }

		public ICommand SaveChangesCommand { get; }

		public ReferenceEditorViewModel(INavigator navigator, DentalClinicModel dentalClinicModel) : base(navigator)
		{
			_dentalClinicModel = dentalClinicModel;

			_items = new ObservableCollection<TItem>();

			RefreshCommand = new DelegateCommand(RefreshItemsList);
			AddNewCommand = new DelegateCommand(AddNewItem);
			DeleteCommand = new DelegateCommand(DeleteItem, CanDeleteItem);
			SaveChangesCommand = new DelegateCommand(SaveChanges, CanSaveChanges);
		}

		public async Task HandleClosingAsync()
		{
			var result = await _navigator.DialogCoordinator.ShowSaveChangesDialogAsync(this);
			if (result == MessageDialogResult.Affirmative)
			{
				await SaveChangesCoreAsync();
			}
			HasChanges = false;
		}

		public virtual void HandleEditEnding(object item)
		{
			var model = item as TItem;
			if (model != null)
			{
				model.IsChanged = true;
			}
			HasChanges = true;
		}
		
		protected override async void OnViewLoaded()
		{
			base.OnViewLoaded();

			await LoadInfoAsync();
		}

		protected async Task LoadInfoAsync()
		{
			try
			{
				var items = await GetItemsCoreAsync();

				_items.Clear();
				foreach (var item in items)
				{
					_items.Add(item);
				}
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
			finally
			{
				HasChanges = false;
			}
		}

		private void AddNewItem()
		{
			var newItem = CreateNew();

			_items.Add(newItem);

			CurrentItem = newItem;
			HasChanges = true;

			NewItemAdded?.Invoke();
		}

		private bool CanDeleteItem()
		{
			return _currentItem != null;
		}

		private async void DeleteItem()
		{
			await DeleteItemCoreAsync(_currentItem);
		}

		private bool CanSaveChanges()
		{
			return HasChanges;
		}

		private async void SaveChanges()
		{
			await SaveChangesCoreAsync();
		}

		private async void RefreshItemsList()
		{
			await LoadInfoAsync();
		}

		protected List<TItem> GetChangedItems()
		{
			List<TItem> changedItems = new List<TItem>();

			foreach (var item in _items)
			{
				if (item.IsChanged)
					changedItems.Add(item);
			}

			return changedItems;
		}

		protected abstract TItem CreateNew();

		protected abstract Task<CollectionResult<TItem>> GetItemsCoreAsync();

		protected abstract Task DeleteItemCoreAsync(TItem item);

		protected abstract Task SaveChangesCoreAsync();
	}
}