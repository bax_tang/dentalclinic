﻿using System;
using System.Windows;
using System.Windows.Mvvm;

using MahApps.Metro.Controls.Dialogs;

namespace DentalClinic.ViewModels
{
	public abstract class DentalClinicViewModel : ViewModelBase
	{
		protected readonly INavigator _navigator;

		protected internal DentalClinicViewModel(INavigator navigator)
		{
			_navigator = navigator;
		}

		protected override void OnActivating()
		{
			base.OnActivating();

			DialogParticipation.SetRegister(AttachedView as DependencyObject, this);
		}

		protected override void OnViewLoaded()
		{
			base.OnViewLoaded();
		}
	}

	public abstract class DentalClinicModalViewModel : ModalViewModelBase
	{
		protected readonly INavigator _navigator;

		protected internal DentalClinicModalViewModel(INavigator navigator)
		{
			_navigator = navigator;
		}

		protected override void OnActivating()
		{
			base.OnActivating();

			DialogParticipation.SetRegister(AttachedView as DependencyObject, this);
		}

		protected override void OnViewLoaded()
		{
			base.OnViewLoaded();
		}
	}
}