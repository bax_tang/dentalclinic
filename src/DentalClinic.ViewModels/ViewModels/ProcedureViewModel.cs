﻿using System;
using System.Windows.Mvvm;

using DentalClinic.Domain;

namespace DentalClinic.ViewModels
{
	public class ProcedureViewModel : PropertyChangedBase
	{
		private ProcedureModel _procedure;

		public long Id
		{
			get { return _procedure.Id; }
			set
			{
				_procedure.Id = value;
				RaisePropertyChanged();
			}
		}

		public string Name
		{
			get { return _procedure.Name; }
			set
			{
				_procedure.Name = value;
				RaisePropertyChanged();
			}
		}

		public decimal Price
		{
			get { return _procedure.Price; }
			set
			{
				_procedure.Price = value;
				RaisePropertyChanged();
			}
		}

		public long? MedicamentId
		{
			get { return _procedure.MedicamentId; }
			set
			{
				_procedure.MedicamentId = value;
				RaisePropertyChanged();
			}
		}

		public ProcedureModel Procedure
		{
			get { return _procedure; }
		}

		public ProcedureViewModel(ProcedureModel procedure)
		{
			_procedure = procedure;
		}
	}
}