﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Mvvm;

using MahApps.Metro.Controls.Dialogs;

using DentalClinic.Domain;
using DentalClinic.Models;
using DentalClinic.Domain.Results;

namespace DentalClinic.ViewModels
{
	public class MedicamentsViewModel : ReferenceEditorViewModel<MedicamentModel>
	{
		public MedicamentsViewModel(INavigator navigator, DentalClinicModel dentalClinicModel) : base(navigator, dentalClinicModel) { }

		protected override MedicamentModel CreateNew()
		{
			var newMedicament = new MedicamentModel
			{
				Id = -1,
				IsChanged = false,
				IsNew = true
			};
			return newMedicament;
		}

		protected override async Task<CollectionResult<MedicamentModel>> GetItemsCoreAsync()
		{
			return await _dentalClinicModel.GetMedicamentsAsync();
		}

		protected override async Task DeleteItemCoreAsync(MedicamentModel item)
		{
			try
			{
				if (item.Id != -1)
				{
					var message = $"Вы действительно хотите удалить \"{item.Name} ({item.Dosage})\"?";
					var result = await _navigator.DialogCoordinator.ShowQuestionAsync(this, message);
					if (result == MessageDialogResult.Affirmative)
					{
						await _dentalClinicModel.DeleteMedicamentAsync(item);

						Items.Remove(item);
					}
				}
				else
					Items.Remove(item);
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
		}

		protected override async Task SaveChangesCoreAsync()
		{
			try
			{
				List<MedicamentModel> changedItems = GetChangedItems();

				await _dentalClinicModel.UpdateMedicamentsAsync(changedItems);
				await LoadInfoAsync();

				HasChanges = false;
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
		}
	}
}