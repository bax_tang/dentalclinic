﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Mvvm;

using MahApps.Metro.Controls.Dialogs;

using DentalClinic.Domain;
using DentalClinic.Domain.Results;
using DentalClinic.Models;

namespace DentalClinic.ViewModels
{
	public class DiagnosesViewModel : ReferenceEditorViewModel<DiagnosisModel>
	{
		public DiagnosesViewModel(INavigator navigator, DentalClinicModel dentalClinicModel) : base(navigator, dentalClinicModel)
		{
		}

		protected override DiagnosisModel CreateNew()
		{
			var newItem = new DiagnosisModel
			{
				Id = -1,
				IsChanged = false,
				IsNew = true
			};
			return newItem;
		}

		protected override async Task<CollectionResult<DiagnosisModel>> GetItemsCoreAsync()
		{
			return await _dentalClinicModel.GetDiagnosesAsync();
		}

		protected override async Task DeleteItemCoreAsync(DiagnosisModel item)
		{
			if (item.Id != -1)
			{
				var message = $"Вы действительно хотите удалить \"{item.Id}: {item.Name}\"?";
				var result = await _navigator.DialogCoordinator.ShowQuestionAsync(this, message);
				if (result == MessageDialogResult.Affirmative)
				{
					await _dentalClinicModel.DeleteDiagnosisAsync(item);
					Items.Remove(item);
				}
			}
			else
				Items.Remove(item);
		}

		protected override async Task SaveChangesCoreAsync()
		{
			try
			{
				List<DiagnosisModel> changedItems = GetChangedItems();

				await _dentalClinicModel.UpdateDiagnosesAsync(changedItems);
				await LoadInfoAsync();

				HasChanges = false;
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
		}
	}
}