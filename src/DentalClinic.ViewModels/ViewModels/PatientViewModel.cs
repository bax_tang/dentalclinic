﻿using System;
using System.Collections.Generic;
using System.Windows.Mvvm;

using DentalClinic.Domain;

namespace DentalClinic.ViewModels
{
	public class PatientViewModel : PropertyChangedBase
	{
		private readonly FullPatientModel _patientModel;
		private bool _hasChanges;

		public bool HasChanges
		{
			get { return _hasChanges; }
			private set
			{
				_hasChanges = value;
				RaisePropertyChanged();
			}
		}

		public string FirstName
		{
			get { return _patientModel.FirstName; }
			set
			{
				_patientModel.FirstName = value;
				RaisePropertyChanged();

				HasChanges = true;
			}
		}

		public string LastName
		{
			get { return _patientModel.LastName; }
			set
			{
				_patientModel.LastName = value;
				RaisePropertyChanged();

				HasChanges = true;
			}
		}

		public string MiddleName
		{
			get { return _patientModel.MiddleName; }
			set
			{
				_patientModel.MiddleName = value;
				RaisePropertyChanged();

				HasChanges = true;
			}
		}

		public DateTime BirthDate
		{
			get { return _patientModel.BirthDate; }
			set
			{
				_patientModel.BirthDate = value;
				RaisePropertyChanged();

				HasChanges = true;
			}
		}

		public string PolicyNumber
		{
			get { return _patientModel.PolicyNumber; }
			set
			{
				_patientModel.PolicyNumber = value;
				RaisePropertyChanged();

				HasChanges = true;
			}
		}

		public string Address
		{
			get { return _patientModel.Address; }
			set
			{
				_patientModel.Address = value;
				RaisePropertyChanged();

				HasChanges = true;
			}
		}

		public string PhoneNumber
		{
			get { return _patientModel.PhoneNumber; }
			set
			{
				_patientModel.PhoneNumber = value;
				RaisePropertyChanged();

				HasChanges = true;
			}
		}

		public FullPatientModel Patient
		{
			get { return _patientModel; }
		}

		public IList<AppointmentModel> Appointments
		{
			get { return _patientModel.Appointments; }
		}

		public IList<DiagnosisModel> Diagnoses
		{
			get { return _patientModel.Diagnoses; }
		}

		public IList<TreatmentModel> Treatments
		{
			get { return _patientModel.Treatments; }
		}

		public PatientViewModel(FullPatientModel patientModel) : base()
		{
			_patientModel = patientModel;
			_hasChanges = false;
		}
	}
}