﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Mvvm;

using DentalClinic.Common;
using DentalClinic.Domain;
using DentalClinic.Models;

namespace DentalClinic.ViewModels
{
	public class PatientsViewModel : DentalClinicViewModel
	{
		private const int PageSize = 20;

		private readonly DentalClinicModel _dentalClinicModel;

		private ObservableCollection<ShortPatientModel> _patients;
		private ShortPatientModel _selectedPatient;

		private bool _loadingInProgress;
		private bool _fullInfoLoadingInProgress;
		private int _currentPage = -1;
		private int _totalPages = -1;
		private bool _canEditSearchText = true;
		private bool _canResetSearchText = false;
		private string _searchText = string.Empty;
		private FullPatientModel _loadedFullPatient;

		public ObservableCollection<ShortPatientModel> Patients
		{
			get { return _patients; }
			private set
			{
				_patients = value;
				RaisePropertyChanged();
			}
		}

		public ShortPatientModel SelectedPatient
		{
			get { return _selectedPatient; }
			set
			{
				if ((_selectedPatient == null && value != null) ||
					(_selectedPatient != null && value != null && _selectedPatient.Id != value.Id))
				{
					LoadFullPatientInfoAsync(value).DontAwaitButCheckExceptions();
				}

				_selectedPatient = value;
				_dentalClinicModel.SelectedPatient = value;
				RaisePropertyChanged();
			}
		}

		public bool LoadingInProgress
		{
			get { return _loadingInProgress; }
			private set
			{
				if (_loadingInProgress != value)
				{
					_loadingInProgress = value;
					RaisePropertyChanged();
				}
			}
		}

		public bool FullInfoLoadingInProgress
		{
			get { return _fullInfoLoadingInProgress; }
			private set
			{
				if (_fullInfoLoadingInProgress != value)
				{
					_fullInfoLoadingInProgress = value;
					RaisePropertyChanged();
				}
			}
		}

		public int CurrentPage
		{
			get { return _currentPage; }
			private set
			{
				_currentPage = value;
				RaisePropertyChanged();
			}
		}

		public int TotalPages
		{
			get { return _totalPages; }
			private set
			{
				_totalPages = value;
				RaisePropertyChanged();
			}
		}

		public bool CanEditSearchText
		{
			get { return _canEditSearchText; }
			private set
			{
				_canEditSearchText = value;
				RaisePropertyChanged();
			}
		}

		public bool CanResetSearchText
		{
			get { return _canResetSearchText; }
			private set
			{
				_canResetSearchText = value;
				RaisePropertyChanged();
			}
		}

		public string SearchText
		{
			get { return _searchText; }
			set
			{
				if (_searchText != value)
				{
					_searchText = value;

					UpdatePatientsAsync(value).DontAwaitButCheckExceptions();

					RaisePropertyChanged();
				}
			}
		}

		public FullPatientModel LoadedPatient
		{
			get { return _loadedFullPatient; }
			private set
			{
				_loadedFullPatient = value;
				RaisePropertyChanged();
			}
		}

		public PatientsViewModel(INavigator navigator, DentalClinicModel clinicModel) : base(navigator)
		{
			_dentalClinicModel = clinicModel;

			_patients = new ObservableCollection<ShortPatientModel>();

			ResetSearchCommand = new DelegateCommand(ResetSearch, CanResetSearch);

			OpenFirstPageCommand = new DelegateCommand(OpenFirstPageAsync);
			OpenPreviousPageCommand = new DelegateCommand(OpenPreviousPageAsync);
			OpenNextPageCommand = new DelegateCommand(OpenNextPageAsync);
			OpenLastPageCommand = new DelegateCommand(OpenLastPageAsync);

			OpenPatientCardCommand = new DelegateCommand(OpenPatientCard);
		}

		public ICommand ResetSearchCommand { get; }

		public ICommand OpenFirstPageCommand { get; }
		public ICommand OpenPreviousPageCommand { get; }
		public ICommand OpenNextPageCommand { get; }
		public ICommand OpenLastPageCommand { get; }

		public ICommand OpenPatientCardCommand { get; }

		protected override void OnActivating()
		{
			base.OnActivating();

			LoadingInProgress = true;

			LoadPatientsAsync().DontAwaitButCheckExceptions();
		}

		protected override void OnViewLoaded()
		{
			base.OnViewLoaded();

			_dentalClinicModel.NewPatientAdded += HandleNewPatientAdded;
		}

		protected override void OnViewUnloaded()
		{
			base.OnViewUnloaded();

			_dentalClinicModel.NewPatientAdded -= HandleNewPatientAdded;
		}

		private void HandleNewPatientAdded(ShortPatientModel patientModel)
		{
			_patients?.Insert(0, patientModel);
		}

		private bool CanResetSearch() => CanResetSearchText;

		private void ResetSearch()
		{
			CanEditSearchText = true;
			CanResetSearchText = false;
			SearchText = null;

			LoadingInProgress = false;
		}

		private async void OpenFirstPageAsync()
		{
			await LoadPatientsCoreAsync(1, PageSize);
		}

		private async void OpenPreviousPageAsync()
		{
			await LoadPatientsCoreAsync(_currentPage - 1, PageSize);
		}

		private async void OpenNextPageAsync()
		{
			await LoadPatientsCoreAsync(_currentPage + 1, PageSize);
		}

		private async void OpenLastPageAsync()
		{
			await LoadPatientsCoreAsync(_totalPages - 1, PageSize);
		}

		private void OpenPatientCard()
		{
			if (_selectedPatient != null)
			{
				_navigator.Navigation.NavigateToModalViewModel<PatientCardViewModel, ShortPatientModel>(_selectedPatient, true);
			}
			else
			{
				_navigator.DialogCoordinator.ShowMessageAsync(this, "Сообщение", "Пожалуйста, выберите пациента.");
			}
		}

		private async Task LoadFullPatientInfoAsync(ShortPatientModel patientModel)
		{
			if (FullInfoLoadingInProgress)
			{
				do
				{
					await Task.Yield();
				}
				while (FullInfoLoadingInProgress);
			}

			FullInfoLoadingInProgress = true;
			try
			{
				var result = await _dentalClinicModel.GetFullPatientInfoAsync(patientModel.Id, false);
				if (result.Succeeded)
				{
					LoadedPatient = result.Data;
				}
				else
				{
					await _navigator.DialogCoordinator.ShowMessageAsync(this, "Ошибка", result.ErrorMessage);
				}
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
			finally
			{
				FullInfoLoadingInProgress = false;
			}
		}

		private async Task UpdatePatientsAsync(string value)
		{
			if (value == null || value.Length == 0)
			{
				CanEditSearchText = true;
				CanResetSearchText = false;

				await LoadPatientsAsync();
			}
			else if (value.Length >= 2)
			{
				LoadingInProgress = true;

				await Task.Delay(100);

				CanEditSearchText = false;
				CanResetSearchText = true;

				await SearchPatientsAsync();
			}
		}

		private async Task LoadPatientsAsync()
		{
			await LoadPatientsCoreAsync(1, pageSize: PageSize);
		}

		private async Task LoadPatientsCoreAsync(int pageNumber, int pageSize)
		{
			try
			{
				var patients = await _dentalClinicModel.GetPatientsAsync(pageNumber, pageSize);

				CurrentPage = patients.PageNumber;
				TotalPages = patients.PagesCount;

				var collection = _patients;
				collection.Clear();

				foreach (var patient in patients)
				{
					collection.Add(patient);
				}
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
			finally
			{
				LoadingInProgress = false;
			}
		}

		private async Task SearchPatientsAsync()
		{
			try
			{
				var patients = await _dentalClinicModel.SearchPatientsAsync(_searchText, 1, pageSize: PageSize);

				var collection = _patients;
				collection.Clear();

				foreach (var patient in patients)
				{
					collection.Add(patient);
				}
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
			finally
			{
				LoadingInProgress = false;
				CanEditSearchText = true;
			}
		}
	}
}