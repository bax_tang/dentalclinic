﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Mvvm;

namespace DentalClinic.ViewModels
{
	public class ReferencesViewModel : DentalClinicModalViewModel
	{
		public ReferencesViewModel(INavigator navigator) : base(navigator)
		{
			ShowAnalyzesCommand = new DelegateCommand(ShowAnalyzesScreen);
			ShowDiagnosesCommand = new DelegateCommand(ShowDiagnosesScreen);
			ShowDoctorsCommand = new DelegateCommand(ShowDoctorsScreen);
			ShowMedicamentsCommand = new DelegateCommand(ShowMedicamentsScreen);
			ShowProceduresCommand = new DelegateCommand(ShowProceduresScreen);
		}

		public ICommand ShowAnalyzesCommand { get; }

		public ICommand ShowDiagnosesCommand { get; }

		public ICommand ShowDoctorsCommand { get; }

		public ICommand ShowMedicamentsCommand { get; }

		public ICommand ShowProceduresCommand { get; }

		private void ShowAnalyzesScreen()
		{
			_navigator.Navigation.NavigateToModalViewModel<AnalyzesViewModel>();
		}

		private void ShowDiagnosesScreen()
		{
			_navigator.Navigation.NavigateToModalViewModel<DiagnosesViewModel>();
		}

		private void ShowDoctorsScreen()
		{
			_navigator.Navigation.NavigateToModalViewModel<DoctorsViewModel>();
		}

		private void ShowMedicamentsScreen()
		{
			_navigator.Navigation.NavigateToModalViewModel<MedicamentsViewModel>();
		}

		private void ShowProceduresScreen()
		{
			_navigator.Navigation.NavigateToModalViewModel<ProceduresViewModel>();
		}
	}
}