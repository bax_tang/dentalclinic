﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Mvvm;

using MahApps.Metro.Controls;

using DentalClinic.Domain;
using DentalClinic.Models;

namespace DentalClinic.ViewModels
{
	public class AddAppointmentViewModel : DentalClinicModalViewModel, IModalViewModel<AddAppointmentNavigationParameter>
	{
		private readonly DentalClinicModel _dentalClinicModel;

		private ObservableCollection<ShortDoctorModel> _doctors;
		private ShortDoctorModel _selectedDoctor;
		private ObservableCollection<ShortPatientModel> _patients;
		private ShortPatientModel _selectedPatient;
		private DateTime _selectedDate;
		private string _patientDisplayString;
		private bool _canSelectDoctor = true;
		private bool _canSelectPatient = true;

		public AddAppointmentNavigationParameter Parameter { get; set; }

		public string PatientDisplayString
		{
			get { return _patientDisplayString; }
			private set
			{
				if (_patientDisplayString != value)
				{
					_patientDisplayString = value;
					RaisePropertyChanged();
				}
			}
		}

		public bool CanSelectDoctor
		{
			get { return _canSelectDoctor; }
			private set
			{
				if (_canSelectDoctor != value)
				{
					_canSelectDoctor = value;
					RaisePropertyChanged();
				}
			}
		}

		public ObservableCollection<ShortDoctorModel> Doctors
		{
			get { return _doctors; }
			private set
			{
				_doctors = value;
				RaisePropertyChanged();
			}
		}

		public ShortDoctorModel SelectedDoctor
		{
			get { return _selectedDoctor; }
			set
			{
				if (_selectedDoctor != value)
				{
					_selectedDoctor = value;
					RaisePropertyChanged();
				}
			}
		}

		public bool CanSelectPatient
		{
			get { return _canSelectPatient; }
			private set
			{
				if (_canSelectPatient != value)
				{
					_canSelectPatient = value;
					RaisePropertyChanged();
				}
			}
		}

		public ObservableCollection<ShortPatientModel> Patients
		{
			get { return _patients; }
			private set
			{
				_patients = value;
				RaisePropertyChanged();
			}
		}

		public ShortPatientModel SelectedPatient
		{
			get { return _selectedPatient; }
			set
			{
				if (_selectedPatient != value)
				{
					_selectedPatient = value;
					RaisePropertyChanged();
				}
			}
		}

		public DateTime SelectedDate
		{
			get { return _selectedDate; }
			set
			{
				if (_selectedDate != value)
				{
					_selectedDate = value;
					RaisePropertyChanged();
				}
			}
		}

		public AddAppointmentViewModel(INavigator navigator, DentalClinicModel dentalClinicModel) : base(navigator)
		{
			_dentalClinicModel = dentalClinicModel;

			CreateAppointmentCommand = new DelegateCommand(CreateAppointment);
			CancelCommand = new DelegateCommand(Cancel);
		}

		public ICommand CreateAppointmentCommand { get; }

		public ICommand CancelCommand { get; }

		protected override async void OnViewLoaded()
		{
			base.OnViewLoaded();

			try
			{
				await LoadInfoAsync();
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
		}

		private async Task LoadInfoAsync()
		{
			var parameter = Parameter;

			SelectedDate = parameter.AppointmentDate;

			var doctors = await _dentalClinicModel.GetDoctorsAsync();
			Doctors = new ObservableCollection<ShortDoctorModel>(doctors);
			if (parameter.DoctorId.HasValue)
			{
				CanSelectDoctor = false;
				SelectedDoctor = doctors.FirstOrDefault(x => x.Id == parameter.DoctorId);
			}

			var patients = await _dentalClinicModel.GetPatientsAsync(pageSize: 1000);
			Patients = new ObservableCollection<ShortPatientModel>(patients);
			if (parameter.PatientId.HasValue)
			{
				CanSelectPatient = false;
				SelectedPatient = patients.FirstOrDefault(x => x.Id == parameter.PatientId);
			}
		}

		private async void CreateAppointment()
		{
			try
			{
				await _dentalClinicModel.AddAppointmentAsync(SelectedPatient, SelectedDoctor, SelectedDate);

				AttachedView.DialogResult = true;
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
		}

		private void Cancel()
		{
			AttachedView.DialogResult = false;
		}
	}
}