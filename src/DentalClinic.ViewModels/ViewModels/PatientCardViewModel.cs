﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Mvvm;

using MahApps.Metro.Controls.Dialogs;

using DentalClinic.Common;
using DentalClinic.Domain;
using DentalClinic.Models;

namespace DentalClinic.ViewModels
{
	public class PatientCardViewModel : DentalClinicModalViewModel, IModalViewModel<ShortPatientModel>
	{
		private readonly DentalClinicModel _dentalClinicModel;

		private bool _loadingInProgress = false;
		private string _displayString = string.Empty;
		private PatientViewModel _patient;

		public ShortPatientModel Parameter { get; set; }

		public bool LoadingInProgress
		{
			get { return _loadingInProgress; }
			private set
			{
				if (_loadingInProgress != value)
				{
					_loadingInProgress = value;
					RaisePropertyChanged();
					RaisePropertyChanged(nameof(LoadingCompleted));
				}
			}
		}

		public bool LoadingCompleted
		{
			get { return !_loadingInProgress; }
		}

		public string DisplayString
		{
			get { return _displayString; }
			private set
			{
				_displayString = value;
				RaisePropertyChanged();
			}
		}

		public PatientViewModel Patient
		{
			get { return _patient; }
			private set
			{
				if (_patient != value)
				{
					_patient = value;
					RaisePropertyChanged();
				}
			}
		}
		
		public PatientCardViewModel(INavigator navigator, DentalClinicModel dentalClinicModel) : base(navigator)
		{
			_dentalClinicModel = dentalClinicModel;

			AddAppointmentCommand = new DelegateCommand(AddAppointment, CanAddAppointment);
			AddTreatmentsCommand = new DelegateCommand<AppointmentModel>(AddTreatments);
			SavePatientCommand = new DelegateCommand(SavePatientAsync);
		}

		public ICommand AddAppointmentCommand { get; }

		public ICommand AddTreatmentsCommand { get; }

		public ICommand SavePatientCommand { get; }

		protected override void OnViewLoaded()
		{
			base.OnViewLoaded();

			LoadingInProgress = true;

			LoadPatientAsync().DontAwaitButCheckExceptions();
		}

		private async Task LoadPatientAsync()
		{
			try
			{
				if (Parameter.Id == -1)
				{
					Patient = new PatientViewModel(new FullPatientModel
					{
						Id = -1,
						BirthDate = DateTime.Now
					});
					DisplayString = "Добавление пациента";
					return;
				}

				var result = await _dentalClinicModel.GetFullPatientInfoAsync(Parameter.Id, true);
				if (result.Succeeded)
				{
					var patientModel = result.Data;

					DisplayString = string.Format("Карта пациента {0} {1} {2}",
						patientModel.LastName,
						patientModel.FirstName,
						patientModel.MiddleName);

					Patient = new PatientViewModel(patientModel);
				}
				else
				{
					await _navigator.DialogCoordinator.ShowMessageAsync(this, "Сообщение", result.ErrorMessage);

					AttachedView.Close();
				}
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
			finally
			{
				LoadingInProgress = false;

				CommandManager.InvalidateRequerySuggested();
			}
		}

		private void AddAppointment()
		{
			var parameter = new AddAppointmentNavigationParameter
			{
				PatientId = Parameter.Id
			};

			var result = _navigator.Navigation
				.NavigateToModalViewModel<AddAppointmentViewModel, AddAppointmentNavigationParameter>(parameter, true);
			if (result == true)
			{
				LoadPatientAsync().DontAwaitButCheckExceptions();
			}
		}

		private bool CanAddAppointment()
		{
			return (_patient != null) && (_patient.Patient.Id != -1);
		}
		
		private void AddTreatments(AppointmentModel appointment)
		{
			var parameter = new AddTreatmentsNavigationParameter
			{
				Appointment = appointment,
				Patient = Parameter
			};

			var result = _navigator.Navigation.NavigateToModalViewModel<AddTreatmentsViewModel, AddTreatmentsNavigationParameter>(parameter, true);
			if (result == true)
			{
				LoadPatientAsync().DontAwaitButCheckExceptions();
			}
		}

		private async void SavePatientAsync()
		{
			try
			{
				long patientId = await _dentalClinicModel.SavePatientAsync(Patient.Patient);

				LoadingInProgress = true;

				var patientModel = new ShortPatientModel
				{
					Id = patientId,
					FirstName = Patient.FirstName,
					LastName = Patient.LastName,
					MiddleName = Patient.MiddleName,
					PolicyNumber = Patient.PolicyNumber
				};
				Parameter = patientModel;

				await LoadPatientAsync();

				CommandManager.InvalidateRequerySuggested();
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
		}
	}
}