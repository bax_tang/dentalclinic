﻿using System;
using System.Windows;
using System.Windows.Mvvm;

using DentalClinic.Models;

namespace DentalClinic.ViewModels
{
	public class RootViewModel : DentalClinicViewModel
	{
		private readonly DentalClinicModel _clinicModel;

		public RootViewModel(INavigator navigator, DentalClinicModel clinicModel) : base(navigator)
		{
			_clinicModel = clinicModel;
		}
	}
}