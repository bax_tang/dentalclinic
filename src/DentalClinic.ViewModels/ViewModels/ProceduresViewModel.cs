﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Mvvm;

using MahApps.Metro.Controls.Dialogs;

using DentalClinic.Domain;
using DentalClinic.Models;
using DentalClinic.Domain.Results;

namespace DentalClinic.ViewModels
{
	public class ProceduresViewModel : ReferenceEditorViewModel<ProcedureModel>
	{
		private ObservableCollection<ShortMedicamentModel> _medicaments;

		public ObservableCollection<ShortMedicamentModel> Medicaments
		{
			get { return _medicaments; }
			private set
			{
				_medicaments = value;
				RaisePropertyChanged();
			}
		}

		public ProceduresViewModel(INavigator navigator, DentalClinicModel dentalClinicModel) : base(navigator, dentalClinicModel) { }
		
		protected override ProcedureModel CreateNew()
		{
			var newProcedure = new ProcedureModel
			{
				Id = -1,
				IsChanged = false,
				IsNew = true
			};
			return newProcedure;
		}
		
		protected override async Task<CollectionResult<ProcedureModel>> GetItemsCoreAsync()
		{
			Medicaments = await LoadMedicamentsAsync();

			return await _dentalClinicModel.GetProceduresAsync();
		}

		protected override async Task DeleteItemCoreAsync(ProcedureModel item)
		{
			try
			{
				if (item.Id != -1)
				{
					var message = $"Вы действительно хотите удалить процедуру \"{item.Name}\"?";
					var result = await _navigator.DialogCoordinator.ShowQuestionAsync(this, message);
					if (result == MessageDialogResult.Affirmative)
					{
						await _dentalClinicModel.DeleteProcedureAsync(item);

						Items.Remove(item);
					}
				}
				else
					Items.Remove(item);
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
		}

		protected override async Task SaveChangesCoreAsync()
		{
			try
			{
				List<ProcedureModel> changedItems = GetChangedItems();

				await _dentalClinicModel.UpdateProceduresAsync(changedItems);
				await LoadInfoAsync();

				HasChanges = false;
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
		}

		private async Task<ObservableCollection<ShortMedicamentModel>> LoadMedicamentsAsync()
		{
			var medicaments = await _dentalClinicModel.GetMedicamentsInfoAsync();

			var result = new ObservableCollection<ShortMedicamentModel>(medicaments);
			return result;
		}
	}
}