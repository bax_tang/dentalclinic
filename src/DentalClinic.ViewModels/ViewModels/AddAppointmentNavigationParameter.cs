﻿using System;

namespace DentalClinic.ViewModels
{
	public class AddAppointmentNavigationParameter
	{
		public long? DoctorId { get; set; } = null;

		public long? PatientId { get; set; } = null;

		public DateTime AppointmentDate { get; set; }

		public AddAppointmentNavigationParameter()
		{
			var nowDate = DateTime.Now;
			AppointmentDate = nowDate.Date.AddHours(nowDate.Hour);
		}
	}
}