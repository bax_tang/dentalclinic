﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Mvvm;

using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

using DentalClinic.Domain;
using DentalClinic.Models;
using DentalClinic.Domain.Results;

namespace DentalClinic.ViewModels
{
	public class DoctorsViewModel : ReferenceEditorViewModel<FullDoctorModel>
	{
		public DoctorsViewModel(INavigator navigator, DentalClinicModel dentalClinicModel) : base(navigator, dentalClinicModel) { }

		protected override FullDoctorModel CreateNew()
		{
			var newItem = new FullDoctorModel
			{
				Id = -1,
				IsChanged = false,
				IsNew = true
			};

			return newItem;
		}

		protected override async Task<CollectionResult<FullDoctorModel>> GetItemsCoreAsync()
		{
			return await _dentalClinicModel.GetDoctorsFullInfoAsync();
		}

		protected override async Task DeleteItemCoreAsync(FullDoctorModel item)
		{
			try
			{
				if (item.Id != -1)
				{
					var message = $"Вы действительно хотите удалить врача \"{item.LastName} {item.FirstName}\"?";
					var result = await _navigator.DialogCoordinator.ShowQuestionAsync(this, message);
					if (result == MessageDialogResult.Affirmative)
					{
						await _dentalClinicModel.DeleteDoctorAsync(item.Id);

						Items.Remove(item);
					}
				}
				else
					Items.Remove(item);
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
		}

		protected override async Task SaveChangesCoreAsync()
		{
			try
			{
				List<FullDoctorModel> changedItems = GetChangedItems();

				await _dentalClinicModel.UpdateDoctorsAsync(changedItems);
				await LoadInfoAsync();

				HasChanges = false;
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
		}
	}
}