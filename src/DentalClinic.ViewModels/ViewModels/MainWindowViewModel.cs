﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Mvvm;
using System.Windows.Threading;

using MahApps.Metro.Controls.Dialogs;

using DentalClinic.Common;
using DentalClinic.Domain;
using DentalClinic.Models;

namespace DentalClinic.ViewModels
{
	public class MainWindowViewModel : DentalClinicViewModel
	{
		private readonly DatabaseModel _databaseModel;
		private readonly DentalClinicModel _clinicModel;

		private bool _initializationInProgress = true;

		public bool InitializationInProgress
		{
			get { return _initializationInProgress; }
			private set
			{
				if (_initializationInProgress != value)
				{
					_initializationInProgress = value;

					RaisePropertyChanged();
					RaisePropertyChanged(nameof(InitializationCompleted));
				}
			}
		}

		public bool InitializationCompleted => !_initializationInProgress;

		public ICommand ShowAddPatientCommand { get; }

		public ICommand ShowReferencesCommand { get; }

		public ICommand ShowScheduleCommand { get; }

		public ICommand ShowPatientCardCommand { get; }

		public MainWindowViewModel(INavigator navigator, DatabaseModel databaseModel, DentalClinicModel clinicModel) : base(navigator)
		{
			_databaseModel = databaseModel;
			_clinicModel = clinicModel;

			ShowAddPatientCommand = new DelegateCommand(ShowAddPatientScreen);
			ShowReferencesCommand = new DelegateCommand(ShowReferencesScreen);
			ShowScheduleCommand = new DelegateCommand(ShowScheduleScreen);
			ShowPatientCardCommand = new DelegateCommand(ShowPatientCardScreen);
		}

		protected override void OnViewLoaded()
		{
			base.OnViewLoaded();

			Task.Run(PrepareApplicationAsync).DontAwaitButCheckExceptions();
		}

		private void ShowAddPatientScreen()
		{
			var newPatientModel = new ShortPatientModel
			{
				Id = -1
			};

			_navigator.Navigation.NavigateToModalViewModel<PatientCardViewModel, ShortPatientModel>(newPatientModel, true);
		}

		private void ShowReferencesScreen()
		{
			_navigator.Navigation.NavigateToModalViewModel<ReferencesViewModel>();
		}

		private void ShowScheduleScreen()
		{
			_navigator.Navigation.NavigateToModalViewModel<ScheduleViewModel>(true);
		}

		private void ShowPatientCardScreen()
		{
			var selectedPatient = _clinicModel.SelectedPatient;
			if (selectedPatient != null)
			{
				_navigator.Navigation.NavigateToModalViewModel<PatientCardViewModel, ShortPatientModel>(selectedPatient, true);
			}
			else
			{
				_navigator.DialogCoordinator.ShowMessageAsync(this, "Сообщение", "Пожалуйста, выберите пациента.");
			}
		}

		private async Task PrepareApplicationAsync()
		{
			await Task.Yield();

			bool migrated = false;

			try
			{
				migrated = await _databaseModel.MigrateToLatestVersionAsync();
				if (migrated)
				{
					await ExecuteOnUiThreadAsync(NavigateToRootView);
				}
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
			finally
			{
				InitializationInProgress = false;
			}
		}

		private void NavigateToRootView()
		{
			_navigator.Navigation.NavigateToViewModel<PatientsViewModel>();
		}
	}
}