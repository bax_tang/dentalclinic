﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Mvvm;

using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

using DentalClinic.Common;
using DentalClinic.Domain;
using DentalClinic.Models;

namespace DentalClinic.ViewModels
{
	public class AddTreatmentsViewModel : DentalClinicModalViewModel, IModalViewModel<AddTreatmentsNavigationParameter>
	{
		private readonly DentalClinicModel _dentalClinicModel;
		private readonly RazorModel _razorModel;

		private bool _loadingInProgress;
		private ObservableCollection<TreatmentModel> _addedTreatments;
		private ObservableCollection<AnalysisModel> _analyzes;
		private ObservableCollection<DiagnosisModel> _diagnoses;
		private ObservableCollection<ProcedureModel> _procedures;
		private TreatmentModel _selectedTreatment;
		private decimal _overallPrice;
		private bool _saved = false;

		public AddTreatmentsNavigationParameter Parameter { get; set; }

		public ObservableCollection<TreatmentModel> AddedTreatments
		{
			get { return _addedTreatments; }
			private set
			{
				_addedTreatments = value;
				RaisePropertyChanged();
			}
		}

		public ObservableCollection<AnalysisModel> Analyzes
		{
			get { return _analyzes; }
			private set
			{
				_analyzes = value;
				RaisePropertyChanged();
			}
		}

		public ObservableCollection<DiagnosisModel> Diagnoses
		{
			get { return _diagnoses; }
			private set
			{
				_diagnoses = value;
				RaisePropertyChanged();
			}
		}

		public ObservableCollection<ProcedureModel> Procedures
		{
			get { return _procedures; }
			private set
			{
				_procedures = value;
				RaisePropertyChanged();
			}
		}

		public TreatmentModel SelectedTreatment
		{
			get { return _selectedTreatment; }
			set
			{
				if (_selectedTreatment != value)
				{
					_selectedTreatment = value;
					RaisePropertyChanged();
				}
			}
		}

		public decimal OverallPrice
		{
			get { return _overallPrice; }
			private set
			{
				_overallPrice = value;
				RaisePropertyChanged();
			}
		}

		public bool LoadingInProgress
		{
			get { return _loadingInProgress; }
			private set
			{
				_loadingInProgress = value;
				RaisePropertyChanged();
			}
		}

		public bool Saved
		{
			get { return _saved; }
			private set
			{
				_saved = value;
				RaisePropertyChanged();
			}
		}

		public AddTreatmentsViewModel(INavigator navigator, DentalClinicModel dentalClinicModel, RazorModel razorModel) : base(navigator)
		{
			_dentalClinicModel = dentalClinicModel;
			_razorModel = razorModel;

			AddNewRowCommand = new DelegateCommand(AddNewRow);
			SaveTreatmentsCommand = new DelegateCommand(SaveTreatments);
			PrintReceiptCommand = new DelegateCommand(PrintReceipt, CanPrintReceipt);
			ExitDialogCommand = new DelegateCommand(ExitDialog);
		}

		public ICommand AddNewRowCommand { get; }

		public ICommand SaveTreatmentsCommand { get; }

		public ICommand PrintReceiptCommand { get; }

		public ICommand ExitDialogCommand { get; }

		public void HandleAnalysisSelectionChanging(AnalysisModel selectedAnalysis)
		{
			var currentTreatment = SelectedTreatment;
			if (currentTreatment != null)
			{
				currentTreatment.AnalysisPrice = selectedAnalysis.Price;
				currentTreatment.UpdateTotalPrice();
			}

			OverallPrice = _addedTreatments.Sum(x => x.TotalPrice);
		}

		public void HandleProcedureSelectionChanging(ProcedureModel selectedProcedure)
		{
			var currentTreatment = SelectedTreatment;
			if (currentTreatment != null)
			{
				currentTreatment.MedicamentPrice = selectedProcedure.MedicamentPrice;
				currentTreatment.ProcedurePrice = selectedProcedure.Price;
				currentTreatment.UpdateTotalPrice();
			}

			OverallPrice = _addedTreatments.Sum(x => x.TotalPrice);
		}

		protected override void OnViewLoaded()
		{
			base.OnViewLoaded();

			LoadingInProgress = true;

			LoadInfoAsync().DontAwaitButCheckExceptions();
		}

		private void AddNewRow()
		{
			var newTreatment = new TreatmentModel
			{
				Id = -1
			};
			newTreatment.PropertyChanged += HandleTreatmentChanged;

			AddedTreatments.Add(newTreatment);

			SelectedTreatment = newTreatment;
		}

		private void HandleTreatmentChanged(object sender, PropertyChangedEventArgs args)
		{
			Saved = false;
		}

		private bool CanPrintReceipt()
		{
			return _saved;
		}

		private async void PrintReceipt()
		{
			try
			{
				var printDialog = new PrintDialog
				{
					CurrentPageEnabled = false,
					SelectedPagesEnabled = false,
					UserPageRangeEnabled = false
				};

				var result = printDialog.ShowDialog();
				if (result == true)
				{
					var orderModel = await BuildOrderModelAsync();

					string orderFile = _razorModel.GenerateOrder(orderModel);

					var queue = printDialog.PrintQueue;
					var name = string.Format("Order_{0}_{1:ddMMyyyy}", orderModel.Id, DateTime.Now);

					var queueItem = queue.AddJob(name, orderFile, true, printDialog.PrintTicket);

					queue.Commit();
				}
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
		}

		private async void SaveTreatments()
		{
			try
			{
				var treatments = new List<TreatmentModel>(AddedTreatments);

				await _dentalClinicModel.SaveTreatmentsAsync(Parameter.Appointment, treatments);

				await LoadInfoAsync();
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
		}

		private void ExitDialog()
		{
			AttachedView.DialogResult = false;
		}

		private async Task<OrderModel> BuildOrderModelAsync()
		{
			long appointmentId = Parameter.Appointment.Id;
			long doctorId = Parameter.Appointment.DoctorId;
			long patientId = Parameter.Appointment.PatientId;

			var doctor = await _dentalClinicModel.GetDoctorAsync(doctorId);
			var patient = await _dentalClinicModel.GetShortPatientInfoAsync(patientId);

			var orderModel = new OrderModel
			{
				Id = appointmentId,
				Date = DateTime.Now,
				DoctorFullName = doctor.FullName,
				PatientFullName = patient.FullName,
				TotalPrice = OverallPrice
			};

			var items = new List<OrderItemModel>(4);

			foreach (var treatment in _addedTreatments)
			{
				var procedure = Procedures.FirstOrDefault(x => x.Id == treatment.ProcedureId);

				items.Add(new OrderItemModel
				{
					MedicamentName = procedure.MedicamentName,
					ProcedureName = procedure.Name,
					Price = treatment.TotalPrice
				});
			}

			orderModel.Items = items;

			return orderModel;
		}

		private async Task LoadInfoAsync()
		{
			try
			{
				var analyzes = await _dentalClinicModel.GetAnalyzesAsync();
				Analyzes = new ObservableCollection<AnalysisModel>(analyzes);

				var diagnoses = await _dentalClinicModel.GetDiagnosesAsync();
				Diagnoses = new ObservableCollection<DiagnosisModel>(diagnoses);

				var procedures = await _dentalClinicModel.GetProceduresAsync();
				Procedures = new ObservableCollection<ProcedureModel>(procedures);

				var treatments = await _dentalClinicModel.GetTreatmentsAsync(Parameter.Appointment);
				AddedTreatments = new ObservableCollection<TreatmentModel>(treatments);

				Saved = true;
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
			finally
			{
				LoadingInProgress = false;
			}
		}
	}
}