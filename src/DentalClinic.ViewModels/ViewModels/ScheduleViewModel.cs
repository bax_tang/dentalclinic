﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Printing;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Mvvm;

using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

using DentalClinic.Common;
using DentalClinic.Domain;
using DentalClinic.Models;

namespace DentalClinic.ViewModels
{
	public class ScheduleViewModel : DentalClinicModalViewModel
	{
		private readonly DentalClinicModel _dentalClinicModel;
		private readonly RazorModel _razorModel;

		private ObservableCollection<ShortDoctorModel> _doctors;
		private ShortDoctorModel _selectedDoctor;
		private DateTime _scheduleDate = DateTime.Now;
		private bool _loadingInProgress = false;
		private ObservableCollection<AppointmentScheduleModel> _appointments;
		private AppointmentScheduleModel _selectedAppointment;

		public ObservableCollection<AppointmentScheduleModel> Appointments
		{
			get { return _appointments; }
			private set
			{
				if (_appointments != value)
				{
					_appointments = value;
					RaisePropertyChanged();
				}
			}
		}

		public AppointmentScheduleModel SelectedAppointment
		{
			get { return _selectedAppointment; }
			set
			{
				if (_selectedAppointment != value)
				{
					_selectedAppointment = value;
					RaisePropertyChanged();
				}
			}
		}

		public ObservableCollection<ShortDoctorModel> Doctors
		{
			get { return _doctors; }
			private set
			{
				_doctors = value;
				RaisePropertyChanged();
			}
		}

		public ShortDoctorModel SelectedDoctor
		{
			get { return _selectedDoctor; }
			set
			{
				if ((_selectedDoctor == null && value != null) ||
					(_selectedDoctor != null && value != null && _selectedDoctor.Id != value.Id))
				{
					_selectedDoctor = value;
					RaisePropertyChanged();
				}
			}
		}

		public DateTime ScheduleDate
		{
			get { return _scheduleDate; }
			set
			{
				if (_scheduleDate != value)
				{
					_scheduleDate = value;
					RaisePropertyChanged();
				}
			}
		}

		public bool CanSelectDate
		{
			get { return !_loadingInProgress; }
		}

		public bool LoadingInProgress
		{
			get { return _loadingInProgress; }
			private set
			{
				if (_loadingInProgress != value)
				{
					_loadingInProgress = value;
					RaisePropertyChanged();
					RaisePropertyChanged(nameof(CanSelectDate));
				}
			}
		}

		public ScheduleViewModel(INavigator navigator, DentalClinicModel dentalClinicModel, RazorModel razorModel) : base(navigator)
		{
			_dentalClinicModel = dentalClinicModel;
			_razorModel = razorModel;

			AddAppointmentCommand = new DelegateCommand<AppointmentScheduleModel>(AddAppointment);
			DeleteAppointmentCommand = new DelegateCommand<AppointmentScheduleModel>(DeleteAppointment);
			AddTreatmentsCommand = new DelegateCommand<AppointmentScheduleModel>(AddTreatments);
			PrintScheduleCommand = new DelegateCommand(PrintSchedule);
		}

		public ICommand AddAppointmentCommand { get; }

		public ICommand DeleteAppointmentCommand { get; }

		public ICommand AddTreatmentsCommand { get; }

		public ICommand PrintScheduleCommand { get; }

		protected override async void OnViewLoaded()
		{
			base.OnViewLoaded();

			_dentalClinicModel.NewAppointmentAdded += HandleAppointmentAdded;

			await LoadInfoAsync();
		}

		protected override void OnViewUnloaded()
		{
			base.OnViewUnloaded();

			_dentalClinicModel.NewAppointmentAdded -= HandleAppointmentAdded;
		}

		protected override async void RaisePropertyChanged([CallerMemberName] string propertyName = "")
		{
			base.RaisePropertyChanged(propertyName);

			if (string.Equals(propertyName, nameof(ScheduleDate)) ||
				string.Equals(propertyName, nameof(SelectedDoctor)))
			{
				await LoadScheduleAsync();
			}
		}

		private void HandleAppointmentAdded(AppointmentModel newAppointment)
		{
			if (_selectedDoctor != null && newAppointment != null && newAppointment.DoctorId == _selectedDoctor.Id)
			{
				DateTime appointmentDate = newAppointment.Date;

				int index = 0;
				foreach (var appointment in _appointments)
				{
					if (appointment.AppointmentDate >= newAppointment.Date)
					{
						break;
					}
					++index;
				}

				_appointments[index] = new AppointmentScheduleModel
				{
					Appointment = newAppointment,
					AppointmentDate = appointmentDate
				};
			}
		}

		private async void AddTreatments(AppointmentScheduleModel scheduleModel)
		{
			try
			{
				if (scheduleModel.Appointment == null) return;

				var patientId = scheduleModel.Appointment.PatientId;
				var patient = await _dentalClinicModel.GetShortPatientInfoAsync(patientId);

				var parameter = new AddTreatmentsNavigationParameter
				{
					Appointment = scheduleModel.Appointment,
					Patient = patient
				};

				var dialogResult = _navigator.Navigation.NavigateToModalViewModel<AddTreatmentsViewModel, AddTreatmentsNavigationParameter>(parameter, true);
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
		}

		private async void AddAppointment(AppointmentScheduleModel scheduleModel)
		{
			try
			{
				var parameter = new AddAppointmentNavigationParameter
				{
					DoctorId = SelectedDoctor.Id,
					AppointmentDate = scheduleModel.AppointmentDate
				};

				var dialogResult = _navigator.Navigation
					.NavigateToModalViewModel<AddAppointmentViewModel, AddAppointmentNavigationParameter>(parameter, true);
				if (dialogResult == true)
				{
					// 
				}
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
		}

		private async void DeleteAppointment(AppointmentScheduleModel scheduleModel)
		{
			try
			{
				if (scheduleModel.Appointment != null)
				{
					var dialogResult = await ShowConfirmationDialogAsync(scheduleModel.Appointment);
					if (dialogResult == MessageDialogResult.Affirmative)
					{
						await _dentalClinicModel.DeleteAppointmentAsync(scheduleModel.Appointment);

						scheduleModel.Appointment = null;
						RaisePropertyChanged(nameof(scheduleModel.AppointmentExists));
						RaisePropertyChanged(nameof(Appointments));
					}
				}
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
		}

		private async void PrintSchedule()
		{
			try
			{
				var printDialog = new PrintDialog
				{
					CurrentPageEnabled = false,
					SelectedPagesEnabled = false,
					UserPageRangeEnabled = false,
				};

				var result = printDialog.ShowDialog();
				if (result == true)
				{
					var scheduleModel = BuildScheduleModel();

					string scheduleFile = _razorModel.GenerateSchedule(scheduleModel);

					var queue = printDialog.PrintQueue;
					var name = string.Format("Schedule_{0}_{1:ddMMyyyy}", SelectedDoctor.Id, scheduleModel.Date);

					var queueItem = queue.AddJob(name, scheduleFile, true, printDialog.PrintTicket);

					queue.Commit();
				}
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
		}

		private ScheduleModel BuildScheduleModel()
		{
			var scheduleModel = new ScheduleModel
			{
				Date = ScheduleDate,
				DoctorFullName = SelectedDoctor.FullName,
				Appointments = new List<AppointmentScheduleModel>(Appointments)
			};

			return scheduleModel;
		}

		private async Task<MessageDialogResult> ShowConfirmationDialogAsync(AppointmentModel appointment)
		{
			var message = string.Format("Вы действительно хотите удалить запись с номером {0} для пациента {1}?",
				appointment.Id,
				appointment.PatientFullName);

			return await _navigator.DialogCoordinator.ShowQuestionAsync(this, message);
		}

		private async Task LoadInfoAsync()
		{
			await Task.Yield();

			try
			{
				var doctors = await _dentalClinicModel.GetDoctorsAsync();

				Doctors = new ObservableCollection<ShortDoctorModel>(doctors);
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
		}

		private async Task LoadScheduleAsync()
		{
			LoadingInProgress = true;

			try
			{
				if (_selectedDoctor == null) return;

				var scheduleAppointments = await _dentalClinicModel.GetAppointmentsForScheduleAsync(_selectedDoctor.Id, _scheduleDate);

				if (_appointments == null)
				{
					_appointments = new ObservableCollection<AppointmentScheduleModel>();
				}

				SelectedAppointment = null;

				_appointments.Clear();
				foreach (var item in scheduleAppointments)
				{
					_appointments.Add(item);
				}

				RaisePropertyChanged(nameof(Appointments));

				SelectedAppointment = _appointments[0];
				
				CommandManager.InvalidateRequerySuggested();
			}
			catch (Exception exc)
			{
				await _navigator.DialogCoordinator.ShowExceptionDialog(this, exc);
			}
			finally
			{
				LoadingInProgress = false;
			}
		}
	}
}