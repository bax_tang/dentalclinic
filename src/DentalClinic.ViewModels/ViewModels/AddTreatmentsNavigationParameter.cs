﻿using System;

using DentalClinic.Domain;

namespace DentalClinic.ViewModels
{
	public class AddTreatmentsNavigationParameter
	{
		public ShortPatientModel Patient { get; set; }

		public AppointmentModel Appointment { get; set; }
	}
}