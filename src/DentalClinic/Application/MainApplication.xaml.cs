﻿using System;
using System.Windows;
using System.Windows.Navigation;

using MahApps.Metro;
using MahApps.Metro.Controls;

namespace DentalClinic
{
	public partial class MainApplication : Application
	{
		public MainApplication() : base()
		{
			InitializeComponent();
		}

		protected override void OnNavigating(NavigatingCancelEventArgs e)
		{
			base.OnNavigating(e);
		}

		protected override void OnNavigated(NavigationEventArgs e)
		{
			base.OnNavigated(e);
		}

		protected override void OnStartup(StartupEventArgs e)
		{
			base.OnStartup(e);

			var accent = ThemeManager.GetAccent("Blue");
			var theme = ThemeManager.GetAppTheme("BaseLight");

			ThemeManager.ChangeAppStyle(this, accent, theme);
		}
	}
}