﻿using System;
using System.Windows;
using System.Windows.Mvvm;
using System.Windows.Navigation;

using MahApps.Metro.Controls;

using DentalClinic.ViewModels;

namespace DentalClinic
{
	public partial class MainWindow : MetroWindow, IView
	{
		public NavigationService Navigation => mainFrame.NavigationService;

		public MainWindow() : base()
		{
			InitializeComponent();
		}
	}
}