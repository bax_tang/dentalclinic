﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Reflection;
using System.Windows;
using System.Windows.Mvvm;
using System.Windows.Navigation;

using Autofac;
using Autofac.Integration.Wpf;

using MahApps.Metro;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

using NLog;

using DentalClinic.Configuration;
using DentalClinic.DataModel.Framework;
using DentalClinic.Models;
using DentalClinic.Services;
using DentalClinic.ViewModels;
using DentalClinic.Views;

namespace DentalClinic
{
	internal class EntryPoint
	{
		[STAThread]
		internal static int Main(string[] args)
		{
			DatabaseConfig.Configure();

			var mainWindow = new MainWindow();

			using (IContainer container = ConfigureContainer(mainWindow.Navigation))
			{
				var application = container.Resolve<MainApplication>();
				var viewModel = container.Resolve<MainWindowViewModel>();

				viewModel.AttachView(mainWindow);

				application.Run(mainWindow);
			}

			return 0;
		}

		private static IContainer ConfigureContainer(NavigationService navigationService)
		{
			ContainerBuilder builder = new ContainerBuilder();

			builder.Register(CreateLogger)
				.As<ILoggerBase, ILogger>()
				.InstancePerDependency();

			builder.RegisterNavigationService(navigationService);

			builder.RegisterInstance(DialogCoordinator.Instance)
				.As<IDialogCoordinator>()
				.AsSelf()
				.SingleInstance();

			builder.RegisterType<DefaultNavigator>()
				.As<INavigator>()
				.SingleInstance();

			builder.RegisterType<MainApplication>().SingleInstance();

			builder.Register(CreateContext)
				.As<IDisposable, DbContext>()
				.AsSelf()
				.InstancePerDependency();

			builder.RegisterType<DentalClinicDbContextScope<DentalClinicContext>>()
				.As<IDbContextScope>()
				.InstancePerLifetimeScope();

			var serviceType = typeof(ApplicationServiceBase);
			builder.RegisterAssemblyTypes(serviceType.Assembly)
				.Where(serviceType.IsAssignableFrom)
				.AsImplementedInterfaces()
				.AsSelf()
				.SingleInstance();

			var modelType = typeof(ApplicationModelBase);
			builder.RegisterAssemblyTypes(modelType.Assembly)
				.Where(modelType.IsAssignableFrom)
				.AsImplementedInterfaces()
				.AsSelf()
				.SingleInstance();

			builder.RegisterType<MainWindowViewModel>().SingleInstance();

			builder.Bind<RootViewModel, RootView>();
			builder.Bind<PatientsViewModel, PatientsView>();

			builder.BindModal<ReferencesViewModel, ReferencesView>(InstanceLifetime.Single, InstanceLifetime.Single);

			builder.BindModal<AnalyzesViewModel, AnalyzesView>(InstanceLifetime.Transient, InstanceLifetime.Transient);
			builder.BindModal<AddAppointmentViewModel, AddAppointmentView>(InstanceLifetime.Transient, InstanceLifetime.Transient);
			builder.BindModal<DiagnosesViewModel, DiagnosesView>(InstanceLifetime.Transient, InstanceLifetime.Transient);
			builder.BindModal<PatientCardViewModel, PatientCardView>(InstanceLifetime.Transient, InstanceLifetime.Transient);
			builder.BindModal<ScheduleViewModel, ScheduleView>(InstanceLifetime.Transient, InstanceLifetime.Transient);
			builder.BindModal<DoctorsViewModel, DoctorsView>(InstanceLifetime.Transient, InstanceLifetime.Transient);
			builder.BindModal<MedicamentsViewModel, MedicamentsView>(InstanceLifetime.Transient, InstanceLifetime.Transient);
			builder.BindModal<ProceduresViewModel, ProceduresView>(InstanceLifetime.Transient, InstanceLifetime.Transient);
			builder.BindModal<AddTreatmentsViewModel, AddTreatmentsView>(InstanceLifetime.Transient, InstanceLifetime.Transient);

			IContainer container = builder.Build();
			return container;
		}

		private static DentalClinicContext CreateContext(IComponentContext context)
		{
			var databaseContext = new DentalClinicContext();

#if DEBUG
			var logger = context.Resolve<ILogger>();
			databaseContext.Database.Log = logger.Debug;
#endif

			return databaseContext;
		}

		private static Logger CreateLogger(IComponentContext context)
		{
			return LogManager.GetLogger("ApplicationLog");
		}
	}
}