﻿using System;
using System.Windows;
using System.Windows.Mvvm;

using MahApps.Metro.Controls.Dialogs;

namespace DentalClinic.ViewModels
{
	internal class DefaultNavigator : INavigator
	{
		private readonly IDialogCoordinator _dialogCoordinator;
		private readonly INavigationService _navigationService;

		public IDialogCoordinator DialogCoordinator => _dialogCoordinator;

		public INavigationService Navigation => _navigationService;

		public DefaultNavigator(IDialogCoordinator dialogCoordinator, INavigationService navigationService)
		{
			_dialogCoordinator = dialogCoordinator;
			_navigationService = navigationService;
		}
	}
}