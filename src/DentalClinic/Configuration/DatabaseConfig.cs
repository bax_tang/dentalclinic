﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Interception;
using System.Linq;

using DentalClinic.DataModel.Entities;
using DentalClinic.DataModel.Framework;
using DentalClinic.DataModel.Migrations;

namespace DentalClinic.Configuration
{
	internal static class DatabaseConfig
	{
		public static void Configure(bool forceApplyMigrations = false)
		{
			DbInterception.Add(new DentalClinicCollationInterceptor("Cyrillic_General_CI_AS"));

			Database.SetInitializer(new MigrateDatabaseToLatestVersion<DentalClinicContext, DentalClinicMigrationsConfiguration>(true));

			if (forceApplyMigrations)
			{
				using (var context = new DentalClinicContext())
				{
					var patient = context.Set<Patient>().FirstOrDefault();

					context.SaveChanges();
				}
			}
		}
	}
}