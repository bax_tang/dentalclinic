﻿using System;
using System.Data.Entity;

using Autofac;

namespace DentalClinic.DataModel.Framework
{
	internal class DentalClinicDbContextScope<TContext> : IDbContextScope where TContext : DentalClinicContext
	{
		private readonly ILifetimeScope _lifetimeScope;

		public DentalClinicDbContextScope(ILifetimeScope lifetimeScope)
		{
			_lifetimeScope = lifetimeScope;
		}

		public DbContext GetContext()
		{
			return _lifetimeScope.Resolve<TContext>();
		}
	}
}