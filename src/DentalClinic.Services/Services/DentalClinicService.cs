﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using DentalClinic.DataModel.Entities;
using DentalClinic.DataModel.Framework;
using DentalClinic.Domain;
using DentalClinic.Domain.Results;

namespace DentalClinic.Services
{
	public class DentalClinicService : ApplicationServiceBase, IDentalClinicService
	{
		public DentalClinicService(IDbContextScope contextScope) : base(contextScope) { }

		/// <summary>
		/// Выполняет получение списка всех доступных анализов
		/// </summary>
		/// <remarks>Публичный метод для получения списка записей из таблицы Анализы</remarks>
		/// <returns></returns>
		public async Task<CollectionResult<AnalysisModel>> GetAnalyzesAsync()
		{
			using (var scope = BeginTransactionScope())
			{
				var analyzesQuery = scope.Context.Set<Analysis>().AsNoTracking()
					.OrderBy(x => x.Id)
					.Select(x => new AnalysisModel
					{
						Id = x.Id,
						WorkKind = x.WorkKind,
						Price = x.Price,
						CreatedAt = x.CreatedAt,
						IsNew = false,
						IsChanged = false
					});

				var analyzes = await analyzesQuery.ToListAsync();

				return new CollectionResult<AnalysisModel>(analyzes)
				{
					PageNumber = 1,
					PagesCount = 1,
					PageSize = analyzes.Count,
					TotalCount = analyzes.Count
				};
			}
		}

		public async Task DeleteAnalysisAsync(long analysisId)
		{
			using (var scope = BeginTransactionScope())
			{
				// "хитрая магия" - явным образом сообщаем Entity Framework, что сущность с идентификатором analysisId
				// должна быть удалена при сохранении изменений; эквивалентный запрос:
				// DELETE FROM [dbo].[Analyzes] WHERE [Id] = @value;

				var analysisToRemove = new Analysis
				{
					Id = analysisId
				};
				scope.Context.Entry(analysisToRemove).State = EntityState.Deleted;

				await scope.Context.SaveChangesAsync();
			}
		}

		/// <summary>
		/// Выполняет обновление списка анализов
		/// </summary>
		/// <param name="analyzes">Список анализов для обновления</param>
		/// <returns></returns>
		public async Task UpdateAnalyzesAsync(List<AnalysisModel> analyzes)
		{
			using (var scope = BeginTransactionScope())
			{
				var analyzesSet = scope.Context.Set<Analysis>();

				foreach (var analysis in analyzes)
				{
					if (analysis.IsNew)
					{
						analyzesSet.Add(new Analysis
						{
							WorkKind = analysis.WorkKind,
							Price = analysis.Price
						});
					}
					else if (analysis.IsChanged)
					{
						var analysisToUpdate = analyzesSet.Attach(new Analysis
						{
							Id = analysis.Id,
							WorkKind = analysis.WorkKind,
							Price = analysis.Price,
							CreatedAt = analysis.CreatedAt,
							UpdatedAt = DateTime.Now
						});
						scope.Context.Entry(analysisToUpdate).State = EntityState.Modified;
					}
				}

				await scope.Context.SaveChangesAsync();
			}
		}

		/// <summary>
		/// Выполняет получение списка врачей (полная информация)
		/// </summary>
		/// <returns></returns>
		public async Task<CollectionResult<FullDoctorModel>> GetDoctorsFullInfoAsync()
		{
			using (var scope = BeginTransactionScope())
			{
				var doctorsQuery = scope.Context.Set<Doctor>().AsNoTracking()
					.OrderBy(x => x.Id)
					.Select(x => new FullDoctorModel
					{
						Id = x.Id,
						FirstName = x.FirstName,
						MiddleName = x.MiddleName,
						LastName = x.LastName,
						Category = x.Category,
						Seniority = x.Seniority,
						CreatedAt = x.CreatedAt,
						IsChanged = false,
						IsNew = false
					});

				var doctors = await doctorsQuery.ToListAsync();

				return new CollectionResult<FullDoctorModel>(doctors)
				{
					PageNumber = 1,
					PagesCount = 1,
					PageSize = doctors.Count,
					TotalCount = doctors.Count
				};
			}
		}

		/// <summary>
		/// Выполняет получение списка, содержащего краткую информацию о зарегистрированных препаратах
		/// </summary>
		/// <returns></returns>
		/// <remarks>Используется для заполнения списка значений для столбца "Медикамент" на экране "Процедуры"</remarks>
		public async Task<CollectionResult<ShortMedicamentModel>> GetMedicamentsInfoAsync()
		{
			using (var scope = BeginTransactionScope())
			{
				var query = scope.Context.Set<Medicament>().AsNoTracking()
					.OrderBy(x => x.Id)
					.Select(x => new ShortMedicamentModel
					{
						Id = x.Id,
						Name = x.Name,
						Dosage = x.Dosage
					});

				var medicaments = await query.ToListAsync();

				return new CollectionResult<ShortMedicamentModel>(medicaments)
				{
					PageNumber = 1,
					PagesCount = 1,
					PageSize = medicaments.Count,
					TotalCount = medicaments.Count
				};
			}
		}

		/// <summary>
		/// Выполняет получение списка, содержащего полную информацию о зарегистрированных медикаментах
		/// </summary>
		/// <returns></returns>
		public async Task<CollectionResult<MedicamentModel>> GetMedicamentsAsync()
		{
			using (var scope = BeginTransactionScope())
			{
				var query = scope.Context.Set<Medicament>().AsNoTracking()
					.OrderBy(x => x.Id)
					.Select(x => new MedicamentModel
					{
						Id = x.Id,
						Name = x.Name,
						Dosage = x.Dosage,
						Price = x.Price,
						CreatedAt = x.CreatedAt,
						IsChanged = false,
						IsNew = false
					});

				var medicaments = await query.ToListAsync();

				return new CollectionResult<MedicamentModel>(medicaments)
				{
					PageNumber = 1,
					PagesCount = 1,
					PageSize = medicaments.Count,
					TotalCount = medicaments.Count
				};
			}
		}

		public async Task DeleteMedicamentAsync(long medicamentId)
		{
			using (var scope = BeginTransactionScope())
			{
				var medicament = new Medicament
				{
					Id = medicamentId
				};
				scope.Context.Entry(medicament).State = EntityState.Deleted;

				await scope.Context.SaveChangesAsync();
			}
		}

		public async Task UpdateMedicamentsAsync(List<MedicamentModel> medicaments)
		{
			using (var scope = BeginTransactionScope())
			{
				var medicamentsSet = scope.Context.Set<Medicament>();

				foreach (var medicament in medicaments)
				{
					if (medicament.IsNew)
					{
						medicamentsSet.Add(new Medicament
						{
							Name = medicament.Name,
							Dosage = medicament.Dosage,
							Price = medicament.Price
						});
					}
					else if (medicament.IsChanged)
					{
						var medicamentToUpdate = medicamentsSet.Attach(new Medicament
						{
							Id = medicament.Id,
							Name = medicament.Name,
							Dosage = medicament.Dosage,
							Price = medicament.Price,
							CreatedAt = medicament.CreatedAt,
							UpdatedAt = DateTime.Now
						});
						scope.Context.Entry(medicamentToUpdate).State = EntityState.Modified;
					}
				}

				await scope.Context.SaveChangesAsync();
			}
		}

		public async Task<CollectionResult<ProcedureModel>> GetProceduresAsync()
		{
			using (var scope = BeginTransactionScope())
			{
				// DefaultIfEmpty при джойне указывает, что для соединения таблиц будет использоваться LEFT JOIN (вместо INNER JOIN по умолчанию)
				// так как для процедуры может быть не указан медикамент, а получить нужно ВСЕ процедуры
				// в случае с INNER JOIN процедуры без медикаментов не попадут в результирующую выборку

				var query = from p in scope.Context.Set<Procedure>().AsNoTracking()
							join m in scope.Context.Set<Medicament>().AsNoTracking().DefaultIfEmpty() on p.MedicamentId equals m.Id
							orderby p.Id
							select new ProcedureModel
							{
								Id = p.Id,
								Name = p.Name,
								Price = p.Price,
								CreatedAt = p.CreatedAt,
								MedicamentId = p.MedicamentId,
								MedicamentName = (m != null) ? m.Name : "",
								MedicamentDosage = (m != null) ? m.Dosage : "",
								MedicamentPrice = (m != null) ? m.Price : 0,
								IsChanged = false,
								IsNew = false
							};

				var procedures = await query.ToListAsync();

				return new CollectionResult<ProcedureModel>(procedures)
				{
					PageNumber = 1,
					PagesCount = 1,
					PageSize = procedures.Count,
					TotalCount = procedures.Count
				};
			}
		}

		public async Task DeleteProcedureAsync(long procedureId)
		{
			using (var scope = BeginTransactionScope())
			{
				var procedure = new Procedure
				{
					Id = procedureId
				};
				scope.Context.Entry(procedure).State = EntityState.Deleted;

				await scope.Context.SaveChangesAsync();
			}
		}

		public async Task UpdateProceduresAsync(List<ProcedureModel> procedures)
		{
			using (var scope = BeginTransactionScope())
			{
				var proceduresSet = scope.Context.Set<Procedure>();

				foreach (var procedure in procedures)
				{
					if (procedure.IsNew)
					{
						proceduresSet.Add(new Procedure
						{
							Name = procedure.Name,
							Price = procedure.Price,
							MedicamentId = procedure.MedicamentId
						});
					}
					else if (procedure.IsChanged)
					{
						var procedureToUpdate = proceduresSet.Attach(new Procedure
						{
							Id = procedure.Id,
							Name = procedure.Name,
							Price = procedure.Price,
							MedicamentId = procedure.MedicamentId,
							CreatedAt = procedure.CreatedAt,
							UpdatedAt = DateTime.Now
						});
						scope.Context.Entry(procedureToUpdate).State = EntityState.Modified;
					}
				}

				await scope.Context.SaveChangesAsync();
			}
		}

		/// <summary>
		/// Выполняет удаление врача с указанным идентификатором
		/// </summary>
		/// <param name="doctorId"></param>
		/// <returns></returns>
		public async Task DeleteDoctorAsync(long doctorId)
		{
			using (var scope = BeginTransactionScope())
			{
				var doctorToRemove = new Doctor
				{
					Id = doctorId
				};
				scope.Context.Entry(doctorToRemove).State = EntityState.Deleted;

				await scope.Context.SaveChangesAsync();
			}
		}

		/// <summary>
		/// Выполняет обновление списка врачей
		/// </summary>
		/// <param name="doctors"></param>
		/// <returns></returns>
		public async Task UpdateDoctorsAsync(List<FullDoctorModel> doctors)
		{
			using (var scope = BeginTransactionScope())
			{
				var doctorsSet = scope.Context.Set<Doctor>();

				foreach (var doctor in doctors)
				{
					if (doctor.IsNew)
					{
						doctorsSet.Add(new Doctor
						{
							FirstName = doctor.FirstName,
							LastName = doctor.LastName,
							MiddleName = doctor.MiddleName,
							Category = doctor.Category,
							Seniority = doctor.Seniority
						});
					}
					else if (doctor.IsChanged)
					{
						var doctorToUpdate = doctorsSet.Attach(new Doctor
						{
							Id = doctor.Id,
							FirstName = doctor.FirstName,
							LastName = doctor.LastName,
							MiddleName = doctor.MiddleName,
							Category = doctor.Category,
							Seniority = doctor.Seniority,
							CreatedAt = doctor.CreatedAt,
							UpdatedAt = DateTime.Now
						});
						scope.Context.Entry(doctorToUpdate).State = EntityState.Modified;
					}

					await scope.Context.SaveChangesAsync();
				}
			}
		}

		/// <summary>
		/// Выполняет получение информации о диагнозах, зарегистрированных в системе
		/// </summary>
		/// <returns></returns>
		public async Task<CollectionResult<DiagnosisModel>> GetDiagnosesAsync()
		{
			using (var scope = BeginTransactionScope())
			{
				var diagnosesQuery = scope.Context.Set<Diagnosis>().AsNoTracking()
					.OrderBy(x => x.Id)
					.Select(x => new DiagnosisModel
					{
						Id = x.Id,
						Name = x.Name,
						Type = x.Type,
						CreatedAt = x.CreatedAt,
						IsNew = false,
						IsChanged = false
					});

				var diagnoses = await diagnosesQuery.ToListAsync();

				return new CollectionResult<DiagnosisModel>(diagnoses)
				{
					PageNumber = 1,
					PagesCount = 1,
					PageSize = diagnoses.Count,
					TotalCount = diagnoses.Count
				};
			}
		}

		public async Task DeleteDiagnosisAsync(long diagnosisId)
		{
			using (var scope = BeginTransactionScope())
			{
				var diagnosisToRemove = new Diagnosis
				{
					Id = diagnosisId
				};
				scope.Context.Entry(diagnosisToRemove).State = EntityState.Deleted;

				await scope.Context.SaveChangesAsync();
			}
		}

		public async Task UpdateDiagnosesAsync(List<DiagnosisModel> diagnoses)
		{
			using (var scope = BeginTransactionScope())
			{
				var diagnosesSet = scope.Context.Set<Diagnosis>();

				foreach (var diagnosis in diagnoses)
				{
					if (diagnosis.IsNew)
					{
						diagnosesSet.Add(new Diagnosis
						{
							Type = diagnosis.Type,
							Name = diagnosis.Name
						});
					}
					else if (diagnosis.IsChanged)
					{
						var diagnosisToUpdate = diagnosesSet.Attach(new Diagnosis
						{
							Id = diagnosis.Id,
							Type = diagnosis.Type,
							Name = diagnosis.Name,
							CreatedAt = diagnosis.CreatedAt,
							UpdatedAt = DateTime.Now
						});
						scope.Context.Entry(diagnosisToUpdate).State = EntityState.Modified;
					}
				}

				await scope.Context.SaveChangesAsync();
			}
		}

		public async Task<long> AddAppointmentAsync(long patientId, long doctorId, DateTime date)
		{
			using (var scope = BeginTransactionScope())
			{
				var appointmentSet = scope.Context.Set<Appointment>();
				var appointment = appointmentSet.Add(new Appointment
				{
					PatientId = patientId,
					DoctorId = doctorId,
					Date = date,
					CreatedAt = DateTime.Now
				});

				await scope.Context.SaveChangesAsync();

				return appointment.Id;
			}
		}

		public async Task<AppointmentModel> GetAppointmentAsync(long appointmentId)
		{
			using (var scope = BeginTransactionScope())
			{
				var query = from a in scope.Context.Set<Appointment>()
							join p in scope.Context.Set<Patient>() on a.PatientId equals p.Id
							where a.Id == appointmentId
							select new AppointmentModel
							{
								Id = a.Id,
								Date = a.Date,
								DoctorId = a.DoctorId,
								PatientId = a.PatientId,
								PatientFirstName = p.FirstName,
								PatientLastName = p.LastName
							};

				var appointment = await query.FirstOrDefaultAsync();
				return appointment;
			}
		}

		public async Task<List<AppointmentModel>> GetAppointmentsForDoctorAsync(long doctorId, DateTime date)
		{
			using (var scope = BeginTransactionScope())
			{
				var query = from a in scope.Context.Set<Appointment>()
							join p in scope.Context.Set<Patient>() on a.PatientId equals p.Id
							where a.DoctorId == doctorId && DbFunctions.TruncateTime(a.Date) == DbFunctions.TruncateTime(date)
							orderby a.Date ascending
							select new AppointmentModel
							{
								Id = a.Id,
								Date = a.Date,
								DoctorId = a.DoctorId,
								PatientId = a.PatientId,
								PatientFirstName = p.FirstName,
								PatientLastName = p.LastName
							};

				var results = await query.AsNoTracking().ToListAsync();

				return results;
			}
		}

		public async Task DeleteAppointmentAsync(long appointmentId)
		{
			using (var scope = BeginTransactionScope())
			{
				var appointmentToRemove = new Appointment
				{
					Id = appointmentId
				};
				scope.Context.Entry(appointmentToRemove).State = EntityState.Deleted;

				await scope.Context.SaveChangesAsync();
			}
		}

		public async Task<ShortDoctorModel> GetDoctorAsync(long doctorId)
		{
			using (var scope = BeginTransactionScope())
			{
				var baseQuery = scope.Context.Set<Doctor>().AsNoTracking();

				var doctorsQuery = baseQuery
					.Where(x => x.Id == doctorId)
					.Select(x => new ShortDoctorModel
					{
						Id = x.Id,
						FirstName = x.FirstName,
						LastName = x.LastName,
						MiddleName = x.MiddleName,
						Category = x.Category
					});

				var doctor = await doctorsQuery.FirstOrDefaultAsync();
				return doctor;
			}
		}

		/// <summary>
		/// Выполняет получение списка всех врачей клиники
		/// </summary>
		/// <returns></returns>
		public async Task<CollectionResult<ShortDoctorModel>> GetDoctorsAsync()
		{
			using (var scope = BeginTransactionScope())
			{
				var baseQuery = scope.Context.Set<Doctor>().AsNoTracking();

				var query = baseQuery
					.OrderBy(x => x.LastName)
					.Select(x => new ShortDoctorModel
					{
						Id = x.Id,
						FirstName = x.FirstName,
						LastName = x.LastName,
						MiddleName = x.MiddleName,
						Category = x.Category
					});

				var doctors = await query.ToListAsync();

				return new CollectionResult<ShortDoctorModel>(doctors)
				{
					PageNumber = 1,
					PagesCount = 1,
					PageSize = doctors.Count,
					TotalCount = doctors.Count
				};
			}
		}

		/// <summary>
		/// Выполняет получение списка пациентов из базы данных
		/// </summary>
		/// <param name="pageNumber">Номер страницы результатов</param>
		/// <param name="pageSize">Размер страницы результатов</param>
		/// <returns></returns>
		public async Task<CollectionResult<ShortPatientModel>> GetPatientsAsync(int pageNumber = 1, int pageSize = 10)
		{
			using (var scope = BeginTransactionScope())
			{
				int skipCount = (pageNumber - 1) * pageSize;

				var baseQuery = scope.Context.Set<Patient>().AsNoTracking();

				int totalCount = await baseQuery.CountAsync();
				int pagesCount = (int)Math.Ceiling(1.0 * totalCount / pageSize);

				var query = GetDefaultQuery(baseQuery, skipCount, pageSize)
					.Select(x => new ShortPatientModel
					{
						Id = x.Id,
						FirstName = x.FirstName,
						LastName = x.LastName,
						MiddleName = x.MiddleName,
						PolicyNumber = x.PolicyNumber
					});

				var patients = await query.ToListAsync();

				return new CollectionResult<ShortPatientModel>(patients)
				{
					PageNumber = pageNumber,
					PagesCount = pagesCount,
					PageSize = pageSize,
					TotalCount = totalCount
				};
			}
		}

		/// <summary>
		/// Выполняет поиск пациентов в базе данных.
		/// <para>Поиск выполняется по вхождению строки поиска в имя или фамилию пациента.</para>
		/// </summary>
		/// <param name="searchText">Строка для поиска</param>
		/// <param name="pageNumber">Номер страницы результатов</param>
		/// <param name="pageSize">Размер страницы результатов</param>
		/// <returns>Коллекция, содержащая информацию о пациентах, удовлетворяющих условиям поиска</returns>
		public async Task<CollectionResult<ShortPatientModel>> SearchPatientsAsync(string searchText, int pageNumber = 1, int pageSize = 10)
		{
			using (var scope = BeginTransactionScope())
			{
				int skipCount = (pageNumber - 1) * pageSize;

				var baseQuery = scope.Context.Set<Patient>().AsNoTracking();

				var query = GetDefaultQuery(baseQuery, skipCount, pageSize)
					.Where(x => x.LastName.Contains(searchText) || x.FirstName.Contains(searchText));

				int totalCount = await query.CountAsync();
				int pagesCount = (int)Math.Ceiling(1.0 * totalCount / pageSize);

				var patients = await query.Select(CreateShortSelector()).ToListAsync();

				return new CollectionResult<ShortPatientModel>(patients)
				{
					PageNumber = pageNumber,
					PagesCount = pagesCount,
					PageSize = pageSize,
					TotalCount = totalCount
				};
			}
		}

		public async Task<ShortPatientModel> GetShortPatientInfoAsync(long patientId)
		{
			using (var scope = BeginTransactionScope())
			{
				var query = scope.Context.Set<Patient>()
					.AsNoTracking()
					.Where(x => x.Id == patientId)
					.Select(CreateShortSelector());

				var patient = await query.FirstOrDefaultAsync();
				return patient;
			}
		}

		/// <summary>
		/// Выполняет получение полной информации о пациенте с указанным идентификатором
		/// </summary>
		/// <param name="patientId">Идентификатор пациента</param>
		/// <param name="loadExtendedInfo">Определяет, необходимо ли запрашивать расширенную информацию</param>
		/// <returns></returns>
		public async Task<OperationResult<FullPatientModel>> GetPatientAsync(long patientId, bool loadExtendedInfo = false)
		{
			using (var scope = BeginTransactionScope())
			{
				var patient = await GetPatientInfoAsync(scope, patientId);
				if (patient != null)
				{
					if (loadExtendedInfo)
					{
						patient.Appointments = await GetAppointmentsAsync(scope, patientId);
						patient.Diagnoses = await GetDiagnosesAsync(scope, patientId);
						patient.Treatments = await GetTreatmentsAsync(scope, patientId);
					}

					return new OperationResult<FullPatientModel>(patient);
				}

				return new OperationResult<FullPatientModel>($"Пациент с номером '{patientId}' отсутствует в базе данных.");
			}
		}

		public async Task<long> SavePatientAsync(FullPatientModel patientModel)
		{
			using (var scope = BeginTransactionScope())
			{
				var patient = AutoMapper.ModelToPatient(patientModel);

				var patientsSet = scope.Context.Set<Patient>();
				if (patient.Id != -1)
				{
					patient.UpdatedAt = DateTime.Now;
					patientsSet.Attach(patient);
					scope.Context.Entry(patient).State = EntityState.Modified;
				}
				else
				{
					patient.CreatedAt = DateTime.Now;
					patientsSet.Add(patient);
				}

				await scope.Context.SaveChangesAsync();

				return patient.Id;
			}
		}

		public async Task<CollectionResult<TreatmentModel>> GetTreatmentsAsync(long appointmentId)
		{
			using (var scope = BeginTransactionScope())
			{
				var treatmentsQuery = from t in scope.Context.Set<Treatment>()
									  join a in scope.Context.Set<Analysis>() on t.AnalysisId equals a.Id
									  join p in scope.Context.Set<Procedure>() on t.ProcedureId equals p.Id
									  join m in scope.Context.Set<Medicament>() on p.MedicamentId equals m.Id
									  where t.AppointmentId == appointmentId
									  select new TreatmentModel
									  {
										  Id = t.Id,
										  AnalysisId = t.AnalysisId,
										  DiagnosisId = t.DiagnosisId,
										  MedicamentId = p.MedicamentId ?? -1,
										  MedicamentPrice = (m != null) ? m.Price : 0,
										  ProcedureId = t.ProcedureId,
										  ProcedurePrice = p.Price,
										  Comments = t.Comments,
										  CreatedAt = t.CreatedAt
									  };

				var treatments = await treatmentsQuery.ToListAsync();

				return new CollectionResult<TreatmentModel>(treatments)
				{
					PageNumber = 1,
					PagesCount = 1,
					PageSize = treatments.Count,
					TotalCount = treatments.Count
				};
			}
		}

		public async Task SaveTreatmentsAsync(long appointmentId, List<TreatmentModel> treatments)
		{
			using (var scope = BeginTransactionScope())
			{
				var treatmentsSet = scope.Context.Set<Treatment>();

				foreach (var treatment in treatments)
				{
					if (treatment.Id == -1)
					{
						treatmentsSet.Add(new Treatment
						{
							AppointmentId = appointmentId,
							AnalysisId = treatment.AnalysisId,
							DiagnosisId = treatment.DiagnosisId,
							ProcedureId = treatment.ProcedureId,
							Comments = treatment.Comments
						});
					}
					else
					{
						var treatmentToUpdate = treatmentsSet.Attach(new Treatment
						{
							Id = treatment.Id,
							AppointmentId = appointmentId,
							AnalysisId = treatment.AnalysisId,
							DiagnosisId = treatment.DiagnosisId,
							ProcedureId = treatment.ProcedureId,
							Comments = treatment.Comments,
							CreatedAt = treatment.CreatedAt
						});
						scope.Context.Entry(treatmentToUpdate).State = EntityState.Modified;
					}
				}

				await scope.Context.SaveChangesAsync();
			}
		}

		private IQueryable<Patient> GetDefaultQuery(IQueryable<Patient> baseQuery, int skipCount, int takeCount)
		{
			var query = baseQuery
				.OrderBy(x => x.LastName).ThenBy(x => x.FirstName)
				.Skip(skipCount).Take(takeCount);

			return query;
		}

		private async Task<FullPatientModel> GetPatientInfoAsync(ITransactionScope scope, long patientId)
		{
			var patientQuery = from p in scope.Context.Set<Patient>()
							   where p.Id == patientId
							   select new FullPatientModel
							   {
								   Id = p.Id,
								   FirstName = p.FirstName,
								   LastName = p.LastName,
								   MiddleName = p.MiddleName,
								   BirthDate = p.BirthDate,
								   PolicyNumber = p.PolicyNumber,
								   PhoneNumber = p.Phone,
								   Address = p.Address,
								   CreatedAt = p.CreatedAt
							   };

			return await patientQuery.FirstOrDefaultAsync();
		}

		private async Task<List<AppointmentModel>> GetAppointmentsAsync(ITransactionScope scope, long patientId)
		{
			var appointmentsQuery = from a in scope.Context.Set<Appointment>()
									join d in scope.Context.Set<Doctor>() on a.DoctorId equals d.Id
									where a.PatientId == patientId
									orderby a.Date descending
									select new AppointmentModel
									{
										Id = a.Id,
										Date = a.Date,
										DoctorId = a.DoctorId,
										DoctorName = d.FirstName + " " + d.LastName
									};

			return await appointmentsQuery.ToListAsync();
		}

		private async Task<List<DiagnosisModel>> GetDiagnosesAsync(ITransactionScope scope, long patientId)
		{
			var diagnosesQuery = from a in scope.Context.Set<Appointment>()
								 join t in scope.Context.Set<Treatment>() on a.Id equals t.AppointmentId
								 join d in scope.Context.Set<Diagnosis>() on t.DiagnosisId equals d.Id
								 where a.PatientId == patientId
								 orderby a.Date descending
								 select new DiagnosisModel
								 {
									 Id = d.Id,
									 Name = d.Name,
									 Date = a.Date,
									 CreatedAt = t.CreatedAt
								 };

			return await diagnosesQuery.ToListAsync();
		}

		private async Task<List<TreatmentModel>> GetTreatmentsAsync(ITransactionScope scope, long patientId)
		{
			var treatmentsQuery = from a in scope.Context.Set<Appointment>()
								  join t in scope.Context.Set<Treatment>() on a.Id equals t.AppointmentId
								  join p in scope.Context.Set<Procedure>() on t.ProcedureId equals p.Id
								  join m in scope.Context.Set<Medicament>().DefaultIfEmpty() on p.MedicamentId equals m.Id
								  where a.PatientId == patientId
								  orderby a.Date descending
								  select new TreatmentModel
								  {
									  Id = t.Id,
									  Date = a.Date,
									  DoctorId = a.DoctorId,
									  MedicamentId = (m != null) ? m.Id : -1,
									  MedicamentName = (m != null) ? m.Name : "",
									  MedicamentPrice = (m != null) ? m.Price : 0,
									  ProcedureId = t.ProcedureId,
									  ProcedureName = p.Name,
									  ProcedurePrice = p.Price
								  };

			return await treatmentsQuery.ToListAsync();
		}

		private Expression<Func<Patient, ShortPatientModel>> CreateShortSelector()
		{
			return (Patient x) => new ShortPatientModel
			{
				Id = x.Id,
				FirstName = x.FirstName.Trim(),
				LastName = x.LastName.Trim(),
				MiddleName = x.MiddleName.Trim(),
				PolicyNumber = x.PolicyNumber
			};
		}
	}
}