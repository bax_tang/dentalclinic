﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using DentalClinic.Domain;
using DentalClinic.Domain.Results;

namespace DentalClinic.Services
{
	public interface IDentalClinicService
	{
		Task<CollectionResult<AnalysisModel>> GetAnalyzesAsync();

		Task DeleteAnalysisAsync(long analysisId);

		Task UpdateAnalyzesAsync(List<AnalysisModel> analyzes);

		Task<long> AddAppointmentAsync(long patientId, long doctorId, DateTime date);

		Task<AppointmentModel> GetAppointmentAsync(long appointmentId);

		Task<List<AppointmentModel>> GetAppointmentsForDoctorAsync(long doctorId, DateTime date);

		Task DeleteAppointmentAsync(long appointmentId);

		Task<ShortDoctorModel> GetDoctorAsync(long doctorId);

		Task<CollectionResult<ShortDoctorModel>> GetDoctorsAsync();

		Task<CollectionResult<FullDoctorModel>> GetDoctorsFullInfoAsync();

		Task DeleteDoctorAsync(long doctorId);

		Task UpdateDoctorsAsync(List<FullDoctorModel> doctors);

		Task<CollectionResult<DiagnosisModel>> GetDiagnosesAsync();

		Task DeleteDiagnosisAsync(long diagnosisId);

		Task UpdateDiagnosesAsync(List<DiagnosisModel> diagnoses);

		Task<CollectionResult<ShortMedicamentModel>> GetMedicamentsInfoAsync();

		Task<CollectionResult<MedicamentModel>> GetMedicamentsAsync();

		Task DeleteMedicamentAsync(long medicamentId);

		Task UpdateMedicamentsAsync(List<MedicamentModel> medicaments);

		Task<CollectionResult<ProcedureModel>> GetProceduresAsync();

		Task DeleteProcedureAsync(long procedureId);

		Task UpdateProceduresAsync(List<ProcedureModel> procedures);

		Task<CollectionResult<ShortPatientModel>> GetPatientsAsync(int pageNumber = 1, int pageSize = 10);

		Task<CollectionResult<ShortPatientModel>> SearchPatientsAsync(string searchText, int pageNumber = 1, int pageSize = 10);

		Task<ShortPatientModel> GetShortPatientInfoAsync(long patientId);

		Task<OperationResult<FullPatientModel>> GetPatientAsync(long patientId, bool loadExtendedInfo = false);

		Task<long> SavePatientAsync(FullPatientModel patient);

		Task<CollectionResult<TreatmentModel>> GetTreatmentsAsync(long appointmentId);

		Task SaveTreatmentsAsync(long appointmentId, List<TreatmentModel> treatments);
	}
}