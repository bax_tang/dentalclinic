﻿using System;
using System.Threading.Tasks;

namespace DentalClinic.Services
{
	public interface IDatabaseService
	{
		Task<bool> MigrateToLatestVersionAsync();
	}
}