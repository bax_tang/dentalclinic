﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DentalClinic.DataModel.Entities;
using DentalClinic.DataModel.Framework;

namespace DentalClinic.Services
{
	public class DatabaseService : ApplicationServiceBase, IDatabaseService
	{
		public DatabaseService(IDbContextScope contextScope) : base(contextScope) { }

		public async Task<bool> MigrateToLatestVersionAsync()
		{
			var context = _contextScope.GetContext();
			if (context != null)
			{
				await context.Set<Patient>().AnyAsync();
				return true;
			}

			return false;
		}
	}
}