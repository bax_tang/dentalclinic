﻿using System;

using DentalClinic.DataModel.Entities;
using DentalClinic.Domain;

namespace DentalClinic.Services
{
	internal static class AutoMapper
	{
		public static Patient ModelToPatient(FullPatientModel patientModel)
		{
			var patient = new Patient
			{
				Id = patientModel.Id,
				FirstName = patientModel.FirstName,
				MiddleName = patientModel.MiddleName,
				LastName = patientModel.LastName,
				BirthDate = patientModel.BirthDate,
				PolicyNumber = patientModel.PolicyNumber,
				Address = patientModel.Address,
				Phone = patientModel.PhoneNumber,
				CreatedAt = patientModel.CreatedAt,
				UpdatedAt = DateTime.Now
			};

			return patient;
		}
	}
}