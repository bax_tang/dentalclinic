﻿using System;
using System.Data;
using System.Data.Entity;
using System.Threading.Tasks;

using DentalClinic.DataModel.Framework;

namespace DentalClinic.Services
{
	public abstract class ApplicationServiceBase
	{
		protected readonly IDbContextScope _contextScope;

		protected internal ApplicationServiceBase(IDbContextScope contextScope)
		{
			_contextScope = contextScope;
		}

		protected ITransactionScope BeginTransactionScope()
		{
			return BeginTransactionScope(IsolationLevel.ReadCommitted);
		}

		protected ITransactionScope BeginTransactionScope(IsolationLevel isolationLevel)
		{
			var context = _contextScope.GetContext();

			return new TransactionScope(context, isolationLevel);
		}
	}
}