﻿using System;
using System.Data;
using System.Data.Entity;

using DentalClinic.DataModel.Framework;

namespace DentalClinic.Services
{
	internal class TransactionScope : ITransactionScope
	{
		private DbContext _context;
		private DbContextTransaction _transaction;

		public DbContext Context => _context;

		public DbContextTransaction Transaction => _transaction;

		public TransactionScope(DbContext context, IsolationLevel isolationLevel)
		{
			if (context == null)
			{
				throw new ArgumentNullException(nameof(context));
			}
			if (context.Database.CurrentTransaction != null)
			{
				throw new ArgumentException("Active transaction already exists.", nameof(context));
			}

			_context = context;
			_transaction = context.Database.BeginTransaction(isolationLevel);
		}

		void IDisposable.Dispose()
		{
			_transaction?.Commit();
			_transaction = null;

			_context?.Dispose();
			_context = null;
		}
	}
}